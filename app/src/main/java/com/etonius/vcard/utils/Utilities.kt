package com.axelbuzz.malisamajmatrimony.utils

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.os.Bundle
import android.text.Html
import android.util.Log
import android.util.Patterns
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.*

import com.etonius.vcard.R
import com.etonius.vcard.models.MediaModel
import com.etonius.vcard.utils.Constants
import java.net.HttpURLConnection
import java.net.URL
import java.text.DecimalFormat
import java.text.NumberFormat
import java.util.*

/**
 * @author Abhijeet.J
 */
class Utilities {


    fun setScreenWidth(activity: Activity) {

        val display = activity.windowManager.defaultDisplay
        screenWidth = display.width
    }

    fun setScreenHeight(activity: Activity) {
        val display = activity.windowManager.defaultDisplay
        screenHeight = display.height
    }


    fun getScreenWidth(): Int {
        return screenWidth
    }

    fun getScreenHeight(): Int {
        return screenHeight
    }

    private fun showUserReadableToast() {

    }

    interface InternetConnectionListener {
        fun isInternetConnected(isConnected: Boolean)
    }

    companion object {

        var screenHeight: Int = 0
            private set
        var screenWidth: Int = 0
            private set
        var imageHeight: Int = 0
        var imageWidth: Int = 0

        fun showLongToast(context: Context, message: String) {
            val toast = Toast.makeText(context, message, Toast.LENGTH_LONG)
            toast.show()
        }

        fun showSmallToast(context: Context, message: String) {
            val toast = Toast.makeText(context, message, Toast.LENGTH_SHORT)
            toast.show()
        }

        fun showInternetErrorToast(context: Context) {
            Toast.makeText(context, context.getString(R.string.internet_error), Toast.LENGTH_SHORT)
                .show()
        }

        fun showUnsuccessfulToast(context: Context) {
            Toast.makeText(
                context,
                context.getString(R.string.unsuccessful_upload_message),
                Toast.LENGTH_SHORT
            ).show()
        }

        fun getTextFromEditText(editText: EditText): String {


            var value = "" + editText.text
            if (value.trim { it <= ' ' }.length == 0) {
                value = ""
            } else {
                value = value.trim { it <= ' ' }
            }
            return value
        }


        fun isMediaEmpty(mediaModels: List<MediaModel>?): Boolean {

            try {
                if (mediaModels == null || mediaModels.size == 0 || mediaModels[0].getMediaUrl().isEmpty())
                    return true
            } catch (e: Exception) {
                e.printStackTrace()
            }
            return false
        }

        fun getGeocodeFromText(editText: EditText): String {
            var value = "" + editText.text
            if (value.trim { it <= ' ' }.length == 0) {
                value = "0.0,0.0"
            } else {
                value = value.trim { it <= ' ' }
            }
            return value
        }

        fun isValidEmail(target: CharSequence?): Boolean {
            return if (target == null)
                false
            else
                Patterns.EMAIL_ADDRESS.matcher(target).matches()
        }

        fun isValidNumber(target: CharSequence?): Boolean {
            return if (target == null)
                false
            else
                target.length >= 10 && Patterns.PHONE.matcher(target).matches()
        }

        /**
         * When value has to checked whether it is number or email
         *
         * @param username
         * @return
         */
        fun validateEmailAndContact(username: String?): Boolean {

            if (username == null)
                return false

            return if (isValidEmail(username) || isValidNumber(username)) true else false

        }

        /**
         * If email is not null check if valid. If contact is not null check if valid
         *
         * @param email
         * @param contact
         * @return
         */
        fun validateEmailOrContact(email: String?, contact: String?): Boolean {

            return if (email != null && isValidEmail(email)) {
                true
            } else if (contact != null && isValidNumber(contact)) {
                true
            } else {
                false
            }
        }

        fun isInternetAvailable(
            context: Context,
            internetConnectionListener: InternetConnectionListener
        ): Boolean {
            try {
                val cm =
                    context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
                if (cm.activeNetworkInfo == null) {
                    internetConnectionListener.isInternetConnected(false)
                    return false
                }

                val activeNetwork = cm.activeNetworkInfo
                if (activeNetwork != null && activeNetwork.isConnected) {

                    val thread = Thread(Runnable {
                        try {
                            val url = URL("http://www.google.com/")
                            val urlc = url.openConnection() as HttpURLConnection
                            urlc.setRequestProperty("User-Agent", "test")
                            urlc.setRequestProperty("Connection", "close")
                            urlc.connectTimeout = 1000 // mTimeout is in seconds
                            urlc.connect()
                            if (urlc.responseCode == 200) {
                                internetConnectionListener.isInternetConnected(true)
                            } else {
                                internetConnectionListener.isInternetConnected(false)
                            }

                        } catch (e: Exception) {
                            Log.i("warning", "Error checking internet connection", e)
                            internetConnectionListener.isInternetConnected(false)
                        }
                    })
                    thread.start()
                }
            } catch (e: Exception) {
                internetConnectionListener.isInternetConnected(false)
                return false
            }

            return false
        }

        fun isEmpty(editText: EditText): Boolean {
            return if (extractDataFromEditText(editText).trim { it <= ' ' }.isEmpty()) {
                true
            } else false

        }

        fun isEmpty(editText: TextView): Boolean {
            return if (extractDataFromTextView(editText).trim { it <= ' ' }.isEmpty()) {
                true
            } else false

        }

        fun isEmpty(string: String?): Boolean {
            return if (string == null || string.isEmpty() || string.trim { it <= ' ' }.isEmpty()) true else false

        }

        fun isMultiStringEmpty(vararg strings: String): Boolean {
            for (string in strings) {
                if (getString(string).isEmpty()) {
                    return false
                }
            }

            return true
        }

        fun convertFeetandInchesToCentimeter(feet: String, inches: String): Double {

            var totalHeight = feet + "." + inches
            var totalHeightFloat = totalHeight.toFloat()
            var heightInFeet = 0.0
            var heightInInches = 0.0


            return String.format("%.2f", totalHeightFloat * 30.48).toDouble()
        }


        fun convertCentimeterToHeightinInches(d: Double): String {
            var feetPart = 0
            var inchesPart = 0
            if ((d).toString() != null && (d).toString().trim({ it <= ' ' }).length != 0) {
                feetPart = Math.floor((d / 2.54) / 12).toInt()
                println((d / 2.54) - (feetPart * 12))
                inchesPart = Math.ceil((d / 2.54) - (feetPart * 12)).toInt()
            }
            return String.format("%d", inchesPart)
        }

        /*fun convertCentimeterToHeight(d: Double): FeetInchesModel {
            try {
                var heightInFeetInch = d / 30.48
                var heightArray = ("" + heightInFeetInch).split(".")
                return FeetInchesModel(heightArray[0], heightArray[1])
            } catch (e: Exception) {
                e.printStackTrace()
            }
            return FeetInchesModel("0", "0")
        }*/


        fun getAmountValueFromText(editText: EditText): Float {

            var amount = 0f
            if (isEmpty(editText))
                return amount

            val edittextValue = getTextFromEditText(editText)
            if (isEmpty(edittextValue))
                return amount

            amount = java.lang.Float.parseFloat(edittextValue)
            return amount
        }

        fun isEmpty(vararg editTexts: EditText): Boolean {
            for (editText in editTexts) {
                if (getString(editText).isEmpty()) {
                    return true
                }
            }

            return false
        }

        fun extractDataFromEditText(editText: EditText?): String {
            if (editText == null)
                return ""
            val text = "" + editText.text

            return getString(text)
        }

        fun extractDataFromTextView(textView: TextView?): String {
            if (textView == null)
                return ""
            val text = "" + textView.text

            return text.trim { it <= ' ' }
        }

        fun extractDataFromCheckedTextView(checkedTextView: CheckedTextView?): Boolean {
            return checkedTextView?.isChecked ?: false

        }

        fun extractDataFromRadioButton(radioButton: RadioButton?): Boolean {
            return radioButton?.isChecked ?: false

        }

        fun setDataToTextView(textView: TextView?, text: String?) {
            if (textView == null || text == null) {
                textView!!.text = ""
                return
            }

            textView.text = text
        }

        /*fun setText(textView: TextView?, text: String?, viewClickListener: ViewClickListener?) {
            if (textView == null || text == null) {
                textView!!.text = ""
                return
            }
            textView.text = text
            if (viewClickListener == null)
                return

            textView.setOnClickListener { viewClickListener.onViewClick(textView) }
        }*/

        fun setDataAndVisibility(textView: TextView, text: String) {
            if (isEmpty(text)) {
                textView.visibility = View.GONE
                return
            }

            textView.text = text
            textView.visibility = View.VISIBLE
        }

        fun setDataAndVisibility(textView: TextView, label: String, text: String) {
            if (isEmpty(text)) {
                textView.visibility = View.GONE
                return
            }

            textView.text = "$label: $text"
            textView.visibility = View.VISIBLE
        }

        fun setQuantityAndUnit(textView: TextView, quantity: String?, unit: String) {
            if (quantity == null || quantity.trim { it <= ' ' }.length == 0)
                return

            textView.text = quantity + " " + getString(unit)
            textView.visibility = View.VISIBLE
        }

        fun clearText(textView: TextView?) {
            if (textView == null)
                return

            textView.text = ""
        }

        public fun getString(text: String?): String {
            return text?.trim { it <= ' ' } ?: ""

        }

        fun getString(textView: EditText): String {

            return extractDataFromEditText(textView).trim { it <= ' ' }
        }

        fun getAmountString(textView: TextView): String {

            return extractDataFromTextView(textView).replace(Constants.RUPEE_SYMBOL, "")
                .trim { it <= ' ' }
        }

        fun getString(textView: TextView): String {

            return extractDataFromTextView(textView).trim { it <= ' ' }
        }

        fun getString(text: Any?): String {
            return if (text == null) "" else "" + text

        }

        fun showDistance(textView: TextView, distance: Float) {
            if (distance > 0) {
                val decimalFormat = DecimalFormat("#.##")
                textView.text = decimalFormat.format(distance.toDouble()) + " Kms"
            } else {
                textView.visibility = View.GONE
            }
        }

        fun showPercentage(amount: Double): String {
            if (amount == 0.0)
                return ""

            val decimalFormat = DecimalFormat("#.##")
            return decimalFormat.format(amount) + " %"
        }

        fun calculateTax(amount: Double, taxPercent: Double): Double {
            var amount = amount
            if (amount == 0.0)
                return amount

            amount = amount * taxPercent / 100
            return amount
        }

        fun showFormattedPrice(price: Double): String {

            val format = NumberFormat.getCurrencyInstance()
            return format.format(price)
        }

        fun convertDistance(distance: Float): Float {
            return distance / 1000
        }

        fun isListEmpty(list: List<*>?): Boolean {
            return if (list == null || list.size == 0) true else false

        }

        fun getImagePath(imageUrl: String?): String {
            return imageUrl?.replace(" ", "%20") ?: ""

        }

        fun setText(view: View?, text: String?) {
            if (view == null || text == null)
                return

            if (view is EditText)
                view.setText(text)
            if (view is TextView)
                view.text = text
            if (view is CheckBox)
                (view as TextView).text = text
        }

        fun setText(textView: TextView, vararg strings: String) {
            val stringBuilder = StringBuilder()

            for (string in strings) {
                if (isEmpty(string))
                    continue

                if (stringBuilder.length > 0)
                    stringBuilder.append(" ")

                stringBuilder.append(getString(string))
            }

            textView.text = stringBuilder.toString()
        }

        fun setScreenDimensions(context: Context) {
            val metrics = context.resources.displayMetrics
            screenWidth = metrics.widthPixels
            screenHeight = metrics.heightPixels
        }

        fun showQuantityAndUnit(textView: TextView?, quantity: String, unit: String) {
            if (textView == null || isEmpty(quantity) || isEmpty(unit)) {
                textView!!.visibility = View.GONE
                return
            } else {
                textView.text = "$quantity $unit"
                textView.visibility = View.VISIBLE
            }
        }

        fun getInt(string: String): Int {
            return if (isEmpty(string)) 0 else Integer.parseInt(string)

        }

        //    public static void setPagerIndicatorColor(CirclePageIndicator circlePageIndicator) {
        //        circlePageIndicator.setFillColor(R.color.colorAccentDark);
        //        circlePageIndicator.setStrokeColor(R.color.gray);
        //    }

        fun getFloatFromEdittext(editText: EditText): Float {
            var `val` = 0f
            `val` = java.lang.Float.parseFloat(extractDataFromEditText(editText))
            return `val`
        }

        fun getIntegerFromEdittext(editText: EditText): Int {
            var `val` = 0
            `val` = Integer.parseInt(extractDataFromEditText(editText))
            return `val`
        }

        fun hideKeyboard(context: Context, view: View) {
            val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }

        fun escapeAllChars(text: String): String {
            var text = text
            text = text.replace("<br />", "\n")
            text = text.replace("&nbsp;", "")
            text = text.replace("t/s", "t's")

            text = text.replace("'", "''")
            text = text.replace(".trim", "")
            text = Html.fromHtml(text).toString()

            return text.trim { it <= ' ' }
        }

        fun isIntentPresent(intent: Intent?): Boolean {
            if (intent == null)
                return false
            return if (intent.extras == null) false else true

        }

        fun isIntentPresent(bundle: Bundle?): Boolean {
            return if (bundle == null) false else true

        }

        fun getAge(dob: String): Int {
            if (isEmpty(dob)) {
                return 0
            }
            val year = Calendar.getInstance().get(Calendar.YEAR)
            val dob1 = dob.split("/".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            val dateOfBirth = dob1[2]
            return year - Integer.parseInt(dateOfBirth)
        }

        fun partiallyHideContact(mobileNumber: String): String {
            val hiddenNumber = StringBuilder()
            hiddenNumber.append(mobileNumber.substring(0, 2))
            hiddenNumber.append("XX")
            hiddenNumber.append(mobileNumber.substring(4, 6))
            hiddenNumber.append("XXXX")
            return hiddenNumber.toString()
        }

        fun partiallyHideEmail(email: String): String {
            val hiddenEmail = StringBuilder()
            for (i in 0 until email.indexOf("@")) {
                hiddenEmail.append("X")
            }
            hiddenEmail.append("@")

            val iStartIndex = email.indexOf("@")
            val iEndIndex = email.indexOf(".")

            for (i in 0 until iEndIndex - iStartIndex - 1) {
                hiddenEmail.append("X")
            }

            hiddenEmail.append(email.substring(iEndIndex))

            return hiddenEmail.toString()
        }

        fun toTitleCase(str: String?): String? {

            if (str == null) {
                return null
            }

            var space = true
            val builder = StringBuilder(str)
            val len = builder.length

            for (i in 0 until len) {
                val c = builder[i]
                if (space) {
                    if (!Character.isWhitespace(c)) {
                        // Convert to title case and switch out of whitespace mode.
                        builder.setCharAt(i, Character.toTitleCase(c))
                        space = false
                    }
                } else if (Character.isWhitespace(c) || c == '.' || c == '!' || c == '?') {
                    space = true
                } else {
                    builder.setCharAt(i, Character.toLowerCase(c))
                }
            }
            return builder.toString()
        }
    }
}