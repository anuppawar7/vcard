package com.etonius.vcard.utils

import android.os.Environment

/**
 * Created by abhijeet on 29/9/15.
 */
interface Constants {


    object IMAGE_REQUEST {
        val CAMERA_REQUEST = 1
        val GALLERY_REQUEST = 2
        val PROFILE_REQUEST = 3
        val VIDEO_REQUEST = 4
        val IMAGES_FOLDER =
            Environment.getExternalStorageDirectory().toString() + "/data/data/vcard/images/"
        val IMAGE_QUALITY = 50
    }

    object FragmentSaveStatus {
        val BasicInfoFragment = "BasicInfoFragment"
        val MoreInfoFragment = "MoreInfoFragment"
        val ProfessionalInfoFragment = "ProfessionalInfoFragment"
        val PersonalIdentificationFragment = "PersonalIdentificationFragment"
        val SaveInstance = true
        val UnSaveInstance = false

    }

    interface OnBoaringFlagsIntent {
        companion object {
            val EditProfile = "2"
            val StartUpProfile = "1"
            val OnBoringFlags = "onBoardingFlag"
        }
    }


    interface ShareableIntents {
        companion object {
            val ProfileType = "profileType"
            val IS_ONLINE = "isOnline"
            val MIN_AGE = "minAge"
            val MAX_AGE = "maxAge"
            val MIN_HEIGHT = "minHeight"
            val MAX_HEIGHT = "maxHeight"
            val MOTHER_TONGUE = "motherTongue"
            val MARRIED_STATUS = "marriedStatus"
            val COUNTRY_ID = "countryId"
            val STATE_ID = "stateId"
            val CITY_ID = "cityId"
            val GENDER = "gender"
            val USER = "user"
        }
    }

    interface PaymentSDKResponse {
        companion object {
            val PAYMENT_SUCCESSFUL = "payment_successfull"
            val PAYMENT_FAILED = "payment_failed"
            val TXN_SESSION_TIMEOUT = "txn_session_timeout"
            val BACK_PRESSED = "back_pressed"
            val USER_CANCELLED = "user_cancelled"
            val ERROR_SERVER_ERROR = "error_server_error"
            val ERROR_NORETRY = "error_noretry"
            val INVAILD_INPUT_DATA = "invalid_input_data"
            val RETRY_FAIL_ERROR = "retry_fail_error"
            val TRXN_NOT_ALLOWED = "trxn_not_allowed"
            val BANK_BACK_PRESS = "bank_back_pressed"
        }
    }

    interface PaymentGetwayTransaction {
        companion object {
            val TxnStatus = "status"
            val Txn_Amout_Debit = "net_amount_debit"
            val Txn_Id = "txnid"
            val Txn_Easepay_Id = "easepayid"
            val Txn_Amount = "amount"
            val Firstname = "firstname"
            val Phone = "phone"
            val Email = "email"
        }
    }

    object SHAREABLE_INTENT {
        val TEST_INTENT = "TestIntent"
        val SELECTED_TEST_INTENT = "SelectedTestIntent"
        val LEGALHEARING_INTENT = "legalHearingIntent"
        val TRAVEL_MODE_INTENT = "TravelModeIntent"
        val TRAVEL_ID_INTENT = "TravelId"
        val SELECTED_TRAVEL_MODE_INTENT = "SelectedTravelModeIntent"
        val CLAIM_MODEL_INTENT = "ClaimModelIntent"
        val CLAIM_ID_INTENT = "ClaimIdIntent"
        val CLAIM_DETAIL_INTENT = "ClaimDetailIntent"
        val CLAIM_TYPES_INTENT = "ClaimTypesIntent"
        val NOTIFICATION_TYPE_ID = "NotificationTypeID"

        var PROFILE_VIEW = 1
        var SOME_ONE_SHORTLISTED = 2
        var REQUEST_CONTACT = 3
        var CONTACT_ALLOW = 4
        var CHAT_MESSAGE = 5
        var SERVER_MESSAGE = 6
        var PAYMENT_REMINDER = 7
        var NEW_PROFILE_ADDED = 8
        var APP_UPDATE = 9
        var OFFERE = 10
        var OTHERS = 11

    }


    interface InstamojoCreds {
        companion object {
            val CLIENT_ID = "aozcso0lAjIR2GHnLN6H0XjFQReAY5a5t3E2ODK9"
            val CLIENT_SEARCH =
                "nmdKY4T0LCZPmWWtoOYGwL9zjgSJY8kZAiKmNo6yrCkRbIluYEJIsPbEyZ2bc4eI4cDe25i4NMmbocCIkzJU2xN2it5BVWasUEDEME3cAjOlX14iO2fAZr2O1znsk5Q7"
        }
    }

    interface UpdateCustomerDetails {
        companion object {
            val Contact = "1"
            val Email = "2"
            val Address = "3"
            val Gender = "4"
            val Ethnicity = "5"
            val DOB = "6"
        }
    }

    object CONTACT_TYPES {
        val Mobile = 1
        val Landline = 2
        val Email = 3
        val Website = 4
    }


    interface NotificationServiceType {
        companion object {

            var PROFILE_VIEW = 1
            var SOME_ONE_SHORTLISTED = 2
            var REQUEST_CONTACT = 3
            var CONTACT_ALLOW = 4
            var CHAT_MESSAGE = 5
            var SERVER_MESSAGE = 6
            var PAYMENT_REMINDER = 7
            var NEW_PROFILE_ADDED = 8
            var APP_UPDATE = 9
            var OFFERE = 10
            var OTHERS = 11
        }
    }


    interface NotificationExtras {
        companion object {
            val HOME = "HomeFr"
        }
        //        String HOME = "HomeFr";
    }

    interface Duration {
        companion object {
            val TODAY = 1
            val YESTERDAY = 2
            val THIS_WEEK = 3
            val THIS_MONTH = 4
            val THIS_YEAR = 5
            val CUSTOM_DATE = 6
        }
    }

    interface UserTypes {
        companion object {
            val Manufacturer = 1
            val Supplier = 2
            val Agency = 3
            val Distributor = 4
            val Retailer = 5
            val Logistics = 6
            val Marketting = 7
            val Operations = 8
            val Customer = 9
            val Employee = 10
        }
    }

    interface AddressTypes {
        companion object {
            val HOME = 1
            val BUSINESS = 2
            val OTHER = 3
        }
    }

    interface EntityTypes {
        companion object {
            val VENDOR = 1
            val CUSTOMER = 2
            val ORDER = 3
            val EVENT = 4
            val STORY = 5
            val MENU = 6
            val SERVICE = 7
            val PACKAGE = 8
            val COMMENT = 9
            val FACILITY = 10
            val CUISINE = 11
            val PRODUCT = 12
            val INTEREST = 13
            val VARIETY = 24

            val RATE = 25
            val OPERATION = 26
            val IMP_DAY = 27
            val PROFILE = 28
            val SOCIETY = 29
            val GEOLOCATION = 31
            val MESSAGE = 32
            val ATTRACTIONS = 33
            val MENU_TYPE = 34
            val CONTACT = 35
            val ADDRESS = 37
            val CATEGORY = 38
            val FUNCTIONS = 39
            val TAG = 40
            val STALL_SIZE = 41
            val SCHEDULE = 42
            val MEETING = 43
            val EVENT_VENDOR = 44
            val MEDIA = 50
            val SALES = 51
            val PAYMENT = 52
        }
    }

    interface MediaType {
        companion object {
            val IMAGE = 1
            val VIDEO = 2
            val THUMBNAIL = 3
            val COVER_PHOTO = 4
            val CARD = 5
            val BROCHURE = 6
        }
    }

    interface MediaCategory {
        companion object {
            val PROFILE = 1
            val GALLERY = 2
            val PACKAGE = 3
            val DISCOUNT = 4
            val OFFER = 5
            val EVENT = 6
            val CUISINE = 7
            val MENU = 8
            val PRODUCT_TYPE = 33
        }
    }

    interface FRAGMENT {
        companion object {
            val HOME = "HomeFr"
            val BOOKINGS = "My Orders"
            val SHORTLISTED = "Shortlisted"
            val REWARD_POINTS = "Reward points"
            val PAYMENTS = "Payments"
            val SETTINGS = "Settings"
            val MY_ACCOUNT = "My Account"
            val REQUEST_VISIT = "Request Visit"
            val SUPPORT = "Support"
            val REFER = "Refer"
            val FEEDBACK = "Feedback"
            val SERVICE_REQUESTS = "Service requests"
            val LOGOUT = "Logout"
            val CREATE_INVITE = "Create Invite"
            val PRODUCTS = "Products"
            val MACHINES = "Machines"
            val MY_ORDERS = "My orders"
        }
    }

    interface OrderStatus {
        companion object {
            val Order_Placed = 1
            val In_Progress = 2
            val Completed = 3
            val Dispatched = 4
            val Closed = 5
            val Cancelled = 6
            val Rescheduled = 7
        }
    }

    interface UserModel {
        companion object {
            val UserId = "UserId"
            val Username = "Username"
            val Password = "Password"
            val DisplayName = "DisplayName"
            val ImageUrl = "ImageUrl"
            val LoginType = "LoginType"
            val SocialType = "SocialType"
            val SocialId = "SocialId"
            val FCM_TOKEN = "FCM_TOKEN"
            val AUTH_TOKEN = "AUTH_TOKEN"
        }
    }

    interface TYPE {
        companion object {
            val TOTAL = "TOTAL"
            val GST = "GST"
            val VAT = "VAT"
        }
    }

    interface LOGIN_STAGE {
        companion object {
            val SIGN_UP = 0
            val LOGIN = 1
            val ENTER_PASSWORD = 2
        }
    }

    interface LoginTypes {
        companion object {
            val Contact = 1
            val Email = 2
            val Google = 3
            val Facebook = 4
        }
    }

    interface UNIT {
        companion object {
            val PERCENT = "Percent"
            val AMOUNT = "Amount"
        }
    }

    object CURRENCY {
        val INR = "INR"
        val USD = "USD"
        val Percent = "%"
    }

    object BookingStatus {
        val DEFAULT = 0
        val APPROVED = 1
        val DECLINED = 2
    }

    object PAYMENT_RESULT_TYPE {
        val APPROVED = 1122
        val DECLINED = 1123
    }

    interface PaymentTypes {
        companion object {
            val Inward = 1
            val Outward = 2
        }
    }

    interface PaymentStatus {
        companion object {
            val Paid = 1
            val Pending = 2
        }
    }

    object SocialMediaParams {
        val EntityTypeId = "EntityTypeId"
        val EntityId = "EntityId"
        val CommentId = "CommentId"
    }

    class USER_TYPE {
        internal var CUSTOMER = "customer"
        internal var EMPLOYEE = "Employee"
    }

    interface FileMediaCategory {
        companion object {
            var PROFILE = "1"
            var GALLERY = "2"
            var DOCUMENT = "3"
            var PACKAGE = "4"
        }
    }

    interface FileMediaType {
        companion object {
            var IMAGE = "1"
            var VIDEO = "2"
            var ThumbNail = "3"
            var COVER_PHOTO = "4"
        }
    }

    interface ATTRIBUTE_ID {
        companion object {
            var Height = "1"
            var SubCaste = "2"
            var Qualification = "3"
            var Marital_Status = "4"
            var AboutMe = "5"
            var Profession = "6"
            var Complexion = "7"
            var AdharCard = "8"
            var SunSign = "9"
            var Expectations = "10"
            var Company_name = "11"
            var Work_location = "12"
        }
    }

    object ContactViewStatus {
        var None = 1
        var Requested = 2
        var Allowed = 3
        var Rejected = 4
        var txtRequested = "Requested"
        var txtRejected = "Rejected"
    }


    object ERROR_HANDLING {
        val NULL = "Null"
        val NO_DATA_FOUND = "No data found"
        val NO_RESULTS_FOUND = "No results found. Please try again"
        val NO_NETWORK1 = "No Network. Please try again"
        val NO_INTERNET = "No Internet"
        val NO_NETWORK2 =
            "Unable to resolve host \"www.functionam.com\": No address associated with hostname"
        val SERVER_ERROR = "Server error"
        val LOGIN_FAILED = "Login failed"
    }

    companion object {

        val CALENDAR_INTENT = "Calendar"
        var UserDetails = "UserDetails"
        val APP_NAME = "MaliSamajMatrimony"
        val CONTACT_NUMBER = "9923449493"
        val RUPEE_SYMBOL = "₹ "
        val DOLLAR_SYMBOL = "$ "
        val VENDOR_ID = "1000"
        val PLATFORM = "Android"
        val CALLBACK_INTENT = "Callback"
        val SCHEDULE_INTENT = "Schedule"
        val LOGOUT = "Logout"
        val ACCESS_TOKEN = "B4BAFE77-7834-4ED4-B040-CCC615C2DC0B20190216101731"
        val NOTIFICATION_INTENT = "Notification"

        val GOOGLE_PLACES_API_KEY = "AIzaSyBHLHMiu5oTTH_uxJIt08FGs_lT8aV5x50"
        val TEXT_QUERY = "textquery"

        val TYPE_NUMBER = 1
        val TYPE_DECIMAL = 2
        val TYPE_CLEAR = 3

        val MEDIA_REQUEST_INTENT = "MediaRequest"

        val MEMBER_ID = "memberId"
        val PACKAGE_ID = "packageId"
        val EMAIL = "email"
        val CONTACT_NO = "contactNo"

        val BOOKING_STATUS_INTENT = "BookingStatus"

        val PAYMENT_RESULT = "PaymentResult"
        val PAYMENT_AMOUNT = "PaymentAmount"

        val USERTYPE_VENDOR = "1"
        val USERTYPE_CUSTOMER = "2"

        val BOOKING_ID_INTENT = "BookingId"
        val TRANSACTION_ID_INTENT = "TransactionId"
        val BOOKING_INTENT = "Booking"
        val LOCATION_INTENT = "Location"
        val PACKAGE_INCLUSIONS_INTENT = "PackageInclusions"
        val MENU_INTENT = "Menu"
        val MENU_TYPE_INTENT = "MenuType"
        val PRODUCT_INTENT = "Product"
        val PRODUCT_ORDER_INTENT = "ProductOrder"
        val CART_INTENT = "CartIntent"
        val ORDER_PRODUCT_INTENT = "OrderProduct"
        val ORDER_INTENT = "Order"
        val PRODUCT_TYPE_INTENT = "ProductType"
        val VARIETY_INTENT = "Variety"
        val SERVICE_INTENT = "Service"
        val RATE_INTENT = "Rate"
        val SALES_ORDER_INTENT = "SalesOrder"
        val QUICK_SELECT_INTENT = "QuickSelect"
        val OPERATIONS_INTENT = "Operations"
        val TAG_INTENT = "Tag"
        val FUNCTION_INTENT = "Function"
        val ENTITY_TYPE_ID_INTENT = "EntityTypeId"
        val ENTITY_ID_INTENT = "EntityId"
        val UNITS_INTENT = "Units"
        val MIN_QUANTITY_INTENT = "MinQuantity"
        val MIN_PASSWORD_LENGTH = 8
        val ATTRIBUTE_INTENT = "Attributes"

        val CONTACT_INTENT = "Contact"

        val MEDIA_SOURCE = "MediaSource"
        val MEDIA_TYPE = "MediaType"
        val MEDIA_CATEGORY = "MediaCategory"
        val MEDIA_UPLOAD = "MediaUpload"

        val IMAGES_FOLDER =
            Environment.getExternalStorageDirectory().toString() + "/myssksamaj/images/"

        val CAMERA_REQUEST = 1
        val COMPANY_LOGO = 2
        val PROFILE_REQUEST = 3
        val DOCUMENT_REQUEST = 4


        val VIDEO_REQUEST = 44

        val MAX_IMAGE_SIZE = 5
        val MAX_NO_OF_IMAGES = 5
        val IMAGE_QUALITY = 50

        /* API's/URLS */
        val UPDATE_IMAGES_API = "updateImage"
        val IMAGE_UPLOAD_API = "/upload_image.php"
        val ROUTE_FRAGMENT_TAG = "route_fragment"

        //    SHARING INTENTS
        val MEDIA_INTENT = "Media"
        val LIST_INDEX = "Index"
        val VENDOR_INTENT = "Vendor"
        val DATE_INTENT = "DateIntent"
        val ADDRESS_INTENT = "Address"
        val ADDRESS_TITLE_INTENT = "AddressTitle"
        val PACKAGE_INTENT = "Package"
        val EVENT_INTENT = "EventIntent"
        val EVENT_ID_INTENT = "EventIdIntent"
        val CATEGORY_INTENT = "Category"
        val STORY_INTENT = "Story"
        val PAYMENT_INTENT = "Payment"

        val candidate = "In my own words"

        val EventTypes = arrayOf(
            "General",
            "Exclusive",
            "Featured",
            "Rated",
            "Promotional",
            "Occasional",
            "Festive"
        )

        val GOOGLE_CHART_API_URL = "http://chart.apis.google.com/chart"
        var QR_API_URL = "?chs=400x400&cht=qr&chl="
        var STORAGE_PERMISSION_CODE = 123
    }
}