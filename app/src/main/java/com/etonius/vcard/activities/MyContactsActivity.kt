package com.etonius.vcard.activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.core.net.toUri
import androidx.recyclerview.widget.RecyclerView
import com.axelbuzz.malisamajmatrimony.utils.Utilities
import com.etonius.vcard.R
import com.etonius.vcard.adapters.CustomListAdapter
import com.etonius.vcard.models.ContactModel
import com.etonius.vcard.viewmodels.ContactViewModel
import java.util.*

class MyContactsActivity : DetailsBaseActivity() {
    var mContext: Context? = null
    var recyclerView: RecyclerView? = null
    var contactList = ArrayList<ContactModel>()

    var edtSearch: EditText? = null
    var adapter: CustomListAdapter? = null
    var imgClear: ImageView? = null
    var contactViewModel: ContactViewModel? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_contacts)
        mContext = this
        setTitle("My Contacts")
        edtSearch = findViewById(R.id.edtHomeSearch)
        recyclerView = findViewById(R.id.recyclerViewMyContacts)
        imgClear = findViewById(R.id.imgMyContactClear)
        // databaseHelper = DatabaseHelper.getInstance(mContext)
        contactViewModel = ContactViewModel(application)
        getSearchList("")

        edtSearch!!.addTextChangedListener(object : TextWatcher {
            var timer = Timer()
            var DELAY: Long = 1000

            override fun afterTextChanged(p0: Editable?) {
                if (p0!!.isNotEmpty()) {
                    timer.cancel()
                    timer = Timer()
                    timer.schedule(object : TimerTask() {
                        override fun run() {
                            getSearchList(p0.toString())
                        }
                    }, DELAY)
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
        })

        imgClear!!.setOnClickListener {
            if (Utilities.isEmpty(edtSearch!!.text.toString())) {
                return@setOnClickListener
            } else {
                edtSearch!!.text.clear()
                getSearchList("")
            }
        }
    }

    fun getSearchList(searchString: String) {

        try {
            this@MyContactsActivity.runOnUiThread {
                //  linearLayout!!.visibility = View.GONE
                Log.e("TAG", "Message $searchString")
                contactList.clear()
                contactList.addAll(contactViewModel!!.getSearchContactResult(searchString))
                if (!Utilities.isListEmpty(contactList)) {
                    adapter = CustomListAdapter(
                        this,
                        recyclerView,
                        contactList,
                        R.layout.row_my_contacts,
                        object : CustomListAdapter.ItemClickedListener {
                            override fun onItemClicked(
                                view: View?,
                                `object`: Any?,
                                position: Int
                            ) {
                                val model = `object` as ContactModel
                                val intent =
                                    Intent(
                                        this@MyContactsActivity,
                                        ContactDetailsActivity::class.java
                                    )
                                intent.putExtra("contactDetails", model)
                                startActivity(intent)
                            }

                            override fun onViewBound(
                                view: View?,
                                `object`: Any?,
                                position: Int
                            ) {
                                val model = `object` as ContactModel
                                val mDisplayName =
                                    view!!.findViewById<TextView>(R.id.tvRowMyContactName)
                                val mDisplayNumber =
                                    view.findViewById<TextView>(R.id.tvRowMyContactNumber)
                                val imgPhoto =
                                    view.findViewById<ImageView>(R.id.imgRomMyContactPhoto)
                                mDisplayName.text = model.contactName
                                mDisplayNumber.text = model.mobileNumber

                                if (Utilities.isEmpty(model.photo)) {
                                    imgPhoto.setImageResource(R.drawable.user)
                                } else {
                                    Log.e("Bitmap", "Data ${model.photo}")
                                    val uri = model.photo.toUri()
                                    imgPhoto.setImageURI(uri)
                                }
                            }
                        })

                    recyclerView!!.adapter = adapter

                } else {
                    // IF CONTACT IS NOT PRESENT IN PHONE DIRECTORY THEN SHOW BUTTON TO SHARE VCARD
                    /*if (searchString.length > 9) {
                        linearLayout!!.visibility = View.VISIBLE
                        btnShare!!.setOnClickListener {
                            var myContact = ContactModel()
                            myContact.mobileNumber = searchString
                            var dialog: BottomSheetFragment? = null
                            val bundle = Bundle()
                            bundle.putSerializable("contactDetails", myContact)
                            dialog = BottomSheetFragment.newInstance()
                            dialog.show(
                                supportFragmentManager,
                                "Custom Bottom Sheet"
                            )
                            dialog.arguments = bundle
                        }
                    } else {
                        linearLayout!!.visibility = View.GONE
                    }*/

                    Toast.makeText(this, "Contact Not available", Toast.LENGTH_SHORT)
                        .show()
                }
            }
        } catch (e: Exception) {

            e.printStackTrace()
        }
    }

}
