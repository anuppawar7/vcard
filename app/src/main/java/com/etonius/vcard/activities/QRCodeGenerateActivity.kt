package com.etonius.vcard.activities

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.AsyncTask
import android.os.Bundle
import android.os.Environment
import android.os.StrictMode
import android.provider.MediaStore
import android.widget.EditText
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.widget.AppCompatButton
import com.axelbuzz.malisamajmatrimony.utils.Utilities
import com.etonius.vcard.R
import com.etonius.vcard.utils.Constants
import com.etonius.vcard.utils.Permissions
import java.io.File
import java.io.FileOutputStream
import java.io.InputStream
import java.io.OutputStream
import java.net.HttpURLConnection
import java.net.URL

class QRCodeGenerateActivity : DetailsBaseActivity() {
    var edtValue: EditText? = null
    var btnDownloadQR: AppCompatButton? = null
    var btnStoreQRToFile: AppCompatButton? = null
    var imgQRCode: ImageView? = null
    var bitmap: Bitmap? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_qrcode_generate)
        setTitle("QR Code Generate")
        edtValue = findViewById(R.id.edtQrValue)
        btnDownloadQR = findViewById(R.id.btnDownloadQR)
        btnStoreQRToFile = findViewById(R.id.btnStoreQRToFile)
        imgQRCode = findViewById(R.id.imgQRCode)
        btnDownloadQR!!.setOnClickListener {
            if (bitmap != null) {
                bitmap!!.recycle()
            }
            imgQRCode!!.setImageResource(android.R.color.transparent)
            val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
            StrictMode.setThreadPolicy(policy)

            var value = edtValue!!.text.toString()
            if (Utilities.isEmpty(value)) {
                Toast.makeText(this, "Please enter value to generate QR Code", Toast.LENGTH_LONG)
                    .show()
                return@setOnClickListener
            }
            var inputStream = loadQRCodeTask(value).execute().get()
            displayQRCode(inputStream)
        }

        btnStoreQRToFile!!.setOnClickListener {

            if (Permissions.Check_STORAGE(this)) {
                saveQRToFile()
            } else {
                Permissions.Request_STORAGE(this, Constants.STORAGE_PERMISSION_CODE)
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == Constants.STORAGE_PERMISSION_CODE) {
            saveQRToFile()
        }
    }

    fun saveQRToFile() {

        try {
            var folderPath =
                File("" + Environment.getExternalStorageDirectory() + File.separator + "VCard/QRCodes")
            if (!folderPath.exists()) {
                folderPath.mkdirs()
            }
            var outPutStream: OutputStream? = null
            var file = File(folderPath, "QRCode_${edtValue!!.text.trim()}.jpg")
            outPutStream = FileOutputStream(file)

            var pictureBitmap = bitmap
            pictureBitmap!!.compress(Bitmap.CompressFormat.JPEG, 100, outPutStream)
            outPutStream.flush()
            outPutStream.close()

            var store = MediaStore.Images.Media.insertImage(
                contentResolver,
                file.absolutePath,
                file.name,
                file.name
            )
            if (!store.isNullOrEmpty()) {
                Toast.makeText(this, "QR Code store successfully..", Toast.LENGTH_SHORT).show()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun displayQRCode(inputStream: InputStream) {
        try {
            bitmap = BitmapFactory.decodeStream(inputStream)
            inputStream.close()
            if (bitmap == null) {
                Toast.makeText(this, "problem in loading QR Code", Toast.LENGTH_SHORT).show()
                return
            }
            imgQRCode!!.setImageBitmap(bitmap)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private inner class loadQRCodeTask(var value: String) : AsyncTask<Void, Void, InputStream>() {

        override fun doInBackground(vararg p0: Void?): InputStream {
            var inputStream: InputStream? = null
            try {
                inputStream = null
                var url = URL(Constants.GOOGLE_CHART_API_URL + Constants.QR_API_URL + value)
                var urlConnection = url.openConnection()

                var httpConn = urlConnection as HttpURLConnection
                httpConn.requestMethod = "GET"
                httpConn.connect()

                if (httpConn.responseCode == HttpURLConnection.HTTP_OK) {
                    inputStream = httpConn.inputStream
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
            return inputStream!!
        }

        override fun onPostExecute(result: InputStream?) {
            super.onPostExecute(result)
        }
    }
}
