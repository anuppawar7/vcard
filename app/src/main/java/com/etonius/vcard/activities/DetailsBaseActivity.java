package com.etonius.vcard.activities;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.etonius.vcard.R;
import com.etonius.vcard.views.MVPView;

public class DetailsBaseActivity extends AppCompatActivity implements MVPView {

    public Context context;
    public Toolbar toolbar;
    private Dialog dialog;
    private ProgressDialog progressDialog;
    private TextView toolbarTitle;
    private ImageView img1, img2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_details_base);
        context = this;
        initUI();
        initToolbar();
    }

    private void initUI() {
        setImmersiveMode();
    }

    public void setTitle(String title) {
        toolbarTitle.setText(title);
    }

    private void initToolbar() {
        try {
            toolbar = (Toolbar) findViewById(R.id.toolbar);
            toolbar.setTitle("");
//        LayoutInflater mInflater = LayoutInflater.from(context);
//        View mCustomView = mInflater.inflate(R.layout.toolbar_custom_view, null);
//        toolbar.addView(mCustomView);

            toolbarTitle = (TextView) findViewById(R.id.toolbar_title);
            toolbarTitle.setText("");
            img1 = (ImageView) findViewById(R.id.img1);
            img2 = (ImageView) findViewById(R.id.img2);
            setSupportActionBar(toolbar);

            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            setSupportActionBar(toolbar);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setImmersiveMode() {
        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
    }

    public void setContentView(int viewId) {
        LinearLayout immediateChild = findViewById(R.id.immediate_child);
        immediateChild.addView(LayoutInflater.from(context).inflate(viewId, null));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        hideLoading();
    }

    @Override
    protected void onPause() {
        super.onPause();
        hideLoading();
    }

    @Override
    public void showLoading() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (dialog == null) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    //View view = getLayoutInflater().inflate(R.layout.progress);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        builder.setView(R.layout.progress);
                    } else
                        builder.setView(LayoutInflater.from(context).inflate(R.layout.progress, null));

                    dialog = builder.create();
                }
                try {
                    dialog.show();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void showLoading(final String message) {
        try {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (progressDialog == null) {
                        progressDialog = new ProgressDialog(context);
                    }
                    progressDialog.setMessage(message);
                    progressDialog.show();
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void hideLoading() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
        });
    }

    @Override
    public void showError(String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    public Toolbar getToolbar() {
        return toolbar;
    }

    public void setToolbar(Toolbar toolbar) {
        this.toolbar = toolbar;
    }

    public ImageView getImg1() {
        return img1;
    }

    public ImageView getImg2() {
        return img2;
    }

    protected void showCartContainer() {
//        showCartContainer.setVisibility(View.VISIBLE);
    }

    protected void hideCartContainer() {
//        showCartContainer.setVisibility(View.GONE);
    }

    protected void setAmount(String amount) {
//        ((TextView) showCartContainer.findViewById(R.id.total_amount)).setText(amount);
    }
}
