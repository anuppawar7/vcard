package com.etonius.vcard.activities

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.TextView
import android.widget.Toast
import com.etonius.vcard.R
import com.etonius.vcard.utils.Constants
import com.etonius.vcard.utils.Permissions
import com.google.zxing.integration.android.IntentIntegrator
import java.util.*

class QRCodeReaderActivity : DetailsBaseActivity() {

    var tvName: TextView? = null
    var tvBusinessName: TextView? = null
    var tvAddress: TextView? = null
    var tvMobileNumber: TextView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_qrcode_reader)
        setTitle("QR Code Reader")
        tvName = findViewById(R.id.tvQRReaderName)
        tvBusinessName = findViewById(R.id.tvQRReaderBName)
        tvAddress = findViewById(R.id.tvQRReaderAddress)
        tvMobileNumber = findViewById(R.id.tvQRReaderNumber)

        if (Permissions.Check_CAMERA(this)) {
            openQRCamera()
        } else {
            Permissions.Request_CAMERA(this, Constants.CAMERA_REQUEST)
        }
    }

    private fun openQRCamera() {
        var intentIntegrator = IntentIntegrator(this)
        intentIntegrator.setPrompt("Scan QR Code")
        intentIntegrator.setBarcodeImageEnabled(true)
        intentIntegrator.initiateScan()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == Constants.CAMERA_REQUEST) {
            openQRCamera()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        var intentResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, data)
        if (intentResult != null) {
            if (intentResult.contents == null) {
                Toast.makeText(this, "Cancelled", Toast.LENGTH_SHORT).show()
                finish()
            } else {
                Log.e("QRCode", "Result" + intentResult)
                val tokens = StringTokenizer(intentResult.contents.toString(), "|")
                var name = tokens.nextToken().trim()
                var businessName = tokens.nextToken().trim()
                var address = tokens.nextToken().trim()
                var number = tokens.nextToken().trim()

                tvName!!.text = name
                tvBusinessName!!.text = businessName
                tvAddress!!.text = address
                tvMobileNumber!!.text = number

            }
        } else {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }
}
