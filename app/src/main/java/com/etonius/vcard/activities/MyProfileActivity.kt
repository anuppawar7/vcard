package com.etonius.vcard.activities

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.CursorLoader
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.os.StrictMode
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import com.axelbuzz.malisamajmatrimony.utils.Utilities
import com.etonius.vcard.R
import com.etonius.vcard.helpers.ImageHelper
import com.etonius.vcard.models.ContactModel
import com.etonius.vcard.utils.Constants
import com.etonius.vcard.utils.ImageUtils
import com.etonius.vcard.viewmodels.ContactViewModel
import com.google.gson.Gson
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

class MyProfileActivity : DetailsBaseActivity() {

    var edtUserName: EditText? = null
    var edtUserNumber: EditText? = null
    var edtUserEmailId: EditText? = null
    var edtCompanyName: EditText? = null
    var edtUserAddress: EditText? = null
    var imgCompanyLogo: ImageView? = null
    var imgMyProfile: ImageView? = null
    var btnUpdate: Button? = null
    var mCameraPhotoPath = ""
    var strProfileImgPath = ""
    var strCompanyLogoImgPath = ""
    val STORAGE_PERMISSION = 555
    val CAMERA_PERMISSION = 1234
    val REQUEST_TAKE_PHOTO = 3
    var UPDATE_PROFILE_REQUEST_CODE = 555
    var IMAGE_VIEW_FLAG: Int? = 0
    var profileGsonString = ""
    var new_profile_Gson_String = ""
    var strImgProfile = ""
    var mContext: Context? = null
    var my_profile_path = ""
    var company_logo_path = ""
    var contactViewModel: ContactViewModel? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_profile)
        mContext = this
        setTitle("Profile")
        edtUserName = findViewById(R.id.edtMyProfile_Name)
        edtUserNumber = findViewById(R.id.edtMyProfile_Number)
        edtUserEmailId = findViewById(R.id.edtMyProfile_Email)
        edtCompanyName = findViewById(R.id.edtMyProfile_CompanyName)
        edtUserAddress = findViewById(R.id.edtMyProfile_Address)
        imgCompanyLogo = findViewById(R.id.imgMyProfile_CompanyLogo)
        imgMyProfile = findViewById(R.id.imgMyProfile_Profile)
        btnUpdate = findViewById(R.id.btnMyProfile_Update)

        contactViewModel = ContactViewModel(application)

        val contactModel = contactViewModel!!.getUserDetails()
        if (!Utilities.isEmpty(contactModel.mobileNumber)) {
            edtUserName!!.setText(contactModel.contactName)
            edtUserNumber!!.setText(contactModel.mobileNumber)
            edtUserEmailId!!.setText(contactModel.emailId)
            edtCompanyName!!.setText(contactModel.companyName)
            edtUserAddress!!.setText(contactModel.address)
            my_profile_path = contactModel.photo
            company_logo_path = contactModel.companyLogo
            ImageHelper.setMediaImage(
                imgMyProfile,
                this,
                contactModel.photo,
                R.drawable.user
            )
            ImageHelper.setMediaImage(
                imgCompanyLogo,
                this,
                contactModel.companyLogo,
                R.drawable.user
            )

            val gson = Gson()
            profileGsonString = gson.toJson(contactModel)
            Log.e("MyProfile", "DB Gson String: $profileGsonString")
        }
        btnUpdate!!.setOnClickListener {
            val model = ContactModel()
            val name = edtUserName!!.text.toString()
            val number = edtUserNumber!!.text.toString()
            val emailId = edtUserEmailId!!.text.toString()
            val companyName = edtCompanyName!!.text.toString()
            val address = edtUserAddress!!.text.toString()

            if (Utilities.isEmpty(name)) {
                Toast.makeText(this, "Please enter name", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if (!Utilities.isValidNumber(number)) {
                Toast.makeText(this, "Please enter valid number", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if (!Utilities.isValidEmail(emailId)) {
                Toast.makeText(this, "Please enter valid email Id", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if (Utilities.isEmpty(companyName)) {
                Toast.makeText(this, "Please enter company name", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            model.contactId = "1"
            model.contactName = name
            model.mobileNumber = number
            model.emailId = emailId
            model.companyName = companyName
            model.address = address

            val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
            StrictMode.setThreadPolicy(policy)

            contactViewModel!!.generateQRCode(model).execute().get()

            if (!Utilities.isEmpty(strCompanyLogoImgPath)) {
                model.companyLogo = strCompanyLogoImgPath
            } else {
                model.companyLogo = contactModel.companyLogo
            }

            if (!Utilities.isEmpty(strProfileImgPath)) {
                model.photo = strProfileImgPath
            } else {
                model.photo = contactModel.photo
            }
            contactViewModel!!.updateUserDetails(model)
            Toast.makeText(this, "Profile Updated...", Toast.LENGTH_SHORT).show()
            var intent = Intent(mContext, HomeActivity::class.java)
            setResult(UPDATE_PROFILE_REQUEST_CODE, intent)
            finish()
        }

        imgMyProfile!!.setOnClickListener {
            IMAGE_VIEW_FLAG = 1
            if (hasPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                showDialogSelectImage(STORAGE_PERMISSION)
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(
                        arrayOf(
                            Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.CAMERA
                        ),
                        STORAGE_PERMISSION
                    )
                }
            }
        }

        imgCompanyLogo!!.setOnClickListener {
            IMAGE_VIEW_FLAG = 2
            if (hasPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                showDialogSelectImage(REQUEST_TAKE_PHOTO)
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(
                        arrayOf(
                            Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.CAMERA
                        ),
                        STORAGE_PERMISSION
                    )
                }
            }
        }

        toolbar.setNavigationOnClickListener {
            onBackPressed()
        }
    }

    fun showDialogSelectImage(request: Int) {
        val inflater = LayoutInflater.from(this)
        val dialogiview = inflater.inflate(R.layout.layout_dialog_option_select_image, null)
        val builder = AlertDialog.Builder(this).create()
        builder.setView(dialogiview)
        val btnCamera = dialogiview.findViewById<LinearLayout>(R.id.liCamera)
        val btnGallery = dialogiview.findViewById<LinearLayout>(R.id.liGallery)
        btnCamera.setOnClickListener {
            openCamera(CAMERA_PERMISSION)
            builder.dismiss()
        }
        btnGallery.setOnClickListener {
            if (hasPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                val i = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
                startActivityForResult(i, request)
                builder.dismiss()
            }
        }
        builder.show()
    }

    fun showExitAlert() {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Update Profile")
        builder.setMessage("Are you sure want to Discard?")

        builder.setPositiveButton("Yes", object : DialogInterface.OnClickListener {
            override fun onClick(p0: DialogInterface?, p1: Int) {
                p0!!.dismiss()
                finish()
            }
        })
        builder.setNegativeButton("No", object : DialogInterface.OnClickListener {
            override fun onClick(p0: DialogInterface?, p1: Int) {
                p0!!.dismiss()
            }
        })
        val alertDialog: AlertDialog = builder.create()
        alertDialog.show()
    }

    override fun onBackPressed() {
        val model = ContactModel()
        model.contactId = "1"
        model.contactName = edtUserName!!.text.toString()
        model.mobileNumber = edtUserNumber!!.text.toString()
        model.emailId = edtUserEmailId!!.text.toString()
        model.companyName = edtCompanyName!!.text.toString()
        model.address = edtUserAddress!!.text.toString()
        if (Utilities.isEmpty(strProfileImgPath)) {
            model.photo = my_profile_path
        } else {
            model.photo = strProfileImgPath
        }
        if (Utilities.isEmpty(strCompanyLogoImgPath)) {
            model.companyLogo = company_logo_path
        } else {
            model.companyLogo = strCompanyLogoImgPath
        }

        try {
            val gson = Gson()
            new_profile_Gson_String = gson.toJson(model)
            Log.e("My Profile", "New Profile String $new_profile_Gson_String")
            Log.e("My Profile", "Old Profile String $profileGsonString")
            if (new_profile_Gson_String == profileGsonString) {
                super.onBackPressed()
            } else {
                showExitAlert()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == STORAGE_PERMISSION) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                showDialogSelectImage(STORAGE_PERMISSION)
            }
        } else if (requestCode == CAMERA_PERMISSION) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                openCamera(CAMERA_PERMISSION)
            }
        }
    }

    fun openCamera(request: Int) {
        if (hasPermission(this, Manifest.permission.CAMERA)) {
            val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            if (takePictureIntent.resolveActivity(this.packageManager) != null) {
                // Create the File where the photo should go
                var photoFile: File? = null
                try {
                    photoFile = createImageFile()
//                photoFile = imageHelper!!.createImageFile(context, mCurrentPhotoPath)
                } catch (ex: IOException) {
                    ex.printStackTrace()
                }
                // Continue only if the File was successfully created
                if (photoFile != null) {
                    try {
                        val photoURI = FileProvider.getUriForFile(
                            this,
                            "com.etonius.vcard",
                            photoFile
                        )
                        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                        startActivityForResult(takePictureIntent, request)
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(
                    arrayOf(
                        Manifest.permission.CAMERA
                    ),
                    CAMERA_PERMISSION
                )
            }
        }
    }

    private fun createImageFile(): File {
        // Create an image file name
        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val imageFileName = "JPEG_" + timeStamp
        val storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        val image = File.createTempFile(
            imageFileName,
            ".jpg",
            storageDir
        )
        // Save a file: path for use with ACTION_VIEW intents
        mCameraPhotoPath = image.absolutePath
        return image
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK || data != null) {
            if (requestCode == Constants.PROFILE_REQUEST) {
                if (resultCode == RESULT_OK) {
                    if (IMAGE_VIEW_FLAG == 1) {
                        try {
                            var strImgPath = ""
                            if (!Utilities.isEmpty(mCameraPhotoPath)) {
                                strImgPath = mCameraPhotoPath!!
                                mCameraPhotoPath = ""
                                strProfileImgPath = ImageUtils.compressImage(
                                    strImgPath, UUID.randomUUID().toString()
                                )
                            } else {
                                val imgUri = data!!.data
                                strImgProfile = getRealPathFromURI(imgUri!!)
                                strProfileImgPath =
                                    ImageUtils.compressImage(
                                        strImgProfile,
                                        UUID.randomUUID().toString()
                                    )
                            }

                            Log.e("TAG", "strProfileImgPath $strProfileImgPath")
                            ImageHelper.setMediaImage(
                                imgMyProfile,
                                this,
                                strProfileImgPath,
                                R.drawable.user
                            )

                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    } else if (IMAGE_VIEW_FLAG == 2) {

                        try {
                            var strImgPath = ""
                            if (!Utilities.isEmpty(mCameraPhotoPath)) {
                                strImgPath = mCameraPhotoPath!!
                                mCameraPhotoPath = ""
                                strCompanyLogoImgPath = ImageUtils.compressImage(
                                    strImgPath, UUID.randomUUID().toString()
                                )
                            } else {
                                val imgUri = data!!.data
                                /*strImgProfile = getRealPathFromURI(imgUri!!)
                                strCompanyLogoImgPath =
                                    ImageUtils.compressImage(strImgProfile, "logo_img")*/
                                strCompanyLogoImgPath = ImageUtils.compressImage(
                                    getRealPathFromURI(imgUri!!),
                                    UUID.randomUUID().toString()
                                )
                            }

                            ImageHelper.setMediaImage(
                                imgCompanyLogo,
                                this,
                                strCompanyLogoImgPath,
                                R.drawable.user
                            )

                            Log.e("TAG", "strProfileImgPath $strCompanyLogoImgPath")

                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }

                }
            }

        }
    }

    private fun getRealPathFromURI(contentUri: Uri): String {
        val proj = arrayOf(MediaStore.Images.Media.DATA)
        val loader = CursorLoader(this, contentUri, proj, null, null, null)
        val cursor = loader.loadInBackground()
        val column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
        cursor.moveToFirst()
        val result = cursor.getString(column_index)
        cursor.close()
        return result
    }

    private fun hasPermission(context: Context, permission: String): Boolean {
        return ContextCompat.checkSelfPermission(
            context,
            permission
        ) == PackageManager.PERMISSION_GRANTED
    }

}
