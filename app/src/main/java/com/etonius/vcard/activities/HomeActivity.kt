package com.etonius.vcard.activities

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.net.toUri
import androidx.recyclerview.widget.RecyclerView
import com.axelbuzz.malisamajmatrimony.utils.Utilities
import com.etonius.vcard.R
import com.etonius.vcard.adapters.CustomListAdapter
import com.etonius.vcard.fragments.BottomSheetFragment
import com.etonius.vcard.helpers.FileOperationsHelper
import com.etonius.vcard.helpers.ImageHelper
import com.etonius.vcard.models.ContactModel
import com.etonius.vcard.utils.Constants
import com.etonius.vcard.utils.Permissions
import com.etonius.vcard.viewmodels.ContactViewModel
import kotlinx.android.synthetic.main.activity_home.*
import java.util.*
import kotlin.collections.ArrayList


class HomeActivity : AppCompatActivity() {
    var locatContactList = ArrayList<ContactModel>()
    val permissionCode = 999
    var UPDATE_PROFILE_REQUEST_CODE = 555
    var mContext: Context? = null
    var tvHomeUserName: TextView? = null
    var tvHomeUserEmail: TextView? = null
    var tvHomeUserNumber: TextView? = null
    var btnShare: Button? = null
    var btnViewQRCode: Button? = null
    var imgHomeClear: ImageView? = null
    var imgHomeUserImage: ImageView? = null
    var imgHomeSync: ImageView? = null
    var imgMyContacts: ImageView? = null
    var imgQRReader: ImageView? = null
    var linearLayout: LinearLayout? = null
    var edtSearch: EditText? = null
    var mRecyclerSearchList: RecyclerView? = null
    var adapter: CustomListAdapter? = null
    var tvUpdateMsg: TextView? = null
    var searchList = ArrayList<ContactModel>()
    var linerLayoutMyProfile: LinearLayout? = null
    var progressBar: ProgressBar? = null
    var contactViewModel: ContactViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        mContext = this
        edtSearch = findViewById(R.id.edtHomeSearch)
        mRecyclerSearchList = findViewById(R.id.recyclerSearchList)
        linearLayout = findViewById(R.id.linearLayoutHome)
        imgHomeClear = findViewById(R.id.imgHomeClear)
        imgHomeSync = findViewById(R.id.imgHomeSync)
        imgMyContacts = findViewById(R.id.imgHomeMyContacts)
        imgQRReader = findViewById(R.id.imgHomeMyQRReader)
        btnShare = findViewById(R.id.btnHomeShare)
        btnViewQRCode = findViewById(R.id.btnViewQRCode)
        tvHomeUserName = findViewById(R.id.tvHomeUserName)
        tvHomeUserEmail = findViewById(R.id.tvHomeUserEmail)
        imgHomeUserImage = findViewById(R.id.imgHomeUserImage)
        tvHomeUserNumber = findViewById(R.id.tvHomeUserNumber)
        tvUpdateMsg = findViewById(R.id.tvHomeUserUpdateMsg)
        linerLayoutMyProfile = findViewById(R.id.linerLayoutMyProfile)
        progressBar = findViewById(R.id.progressBar_home)

        contactViewModel = ContactViewModel(application)
        locatContactList = contactViewModel!!.getContactFromLocal()

        var contactModel = contactViewModel!!.getUserDetails()

        if (!Utilities.isEmpty(contactModel.mobileNumber)) {
            tvUpdateMsg!!.visibility = View.GONE
            tvHomeUserName!!.text = contactModel.contactName
            tvHomeUserNumber!!.text = contactModel.mobileNumber
            Utilities.setDataAndVisibility(tvHomeUserEmail!!, contactModel.emailId)
            if (!Utilities.isEmpty(contactModel.photo)) {
                ImageHelper.setMediaImage(
                    imgHomeUserImage,
                    this,
                    contactModel.photo,
                    R.drawable.user
                )
            }
        } else {
            tvHomeUserNumber!!.visibility = View.GONE
            tvUpdateMsg!!.visibility = View.VISIBLE
        }

        if (Utilities.isListEmpty(locatContactList)) {
            if (Permissions.Check_READ_CONTACTS(this)) {
                contactViewModel!!.getContactFromProvider().execute()
            } else {
                Permissions.Request_READ_CONTACTS(this, permissionCode)
            }
        }
        linerLayoutMyProfile!!.setOnClickListener {
            val i = Intent(this, MyProfileActivity::class.java)
            startActivityForResult(i, UPDATE_PROFILE_REQUEST_CODE)
        }

        imgHomeSync!!.setOnClickListener {
            contactViewModel!!.deleteContacts()
            if (Permissions.Check_READ_CONTACTS(this)) {
                contactViewModel!!.getContactFromProvider().execute()
            } else {
                Permissions.Request_READ_CONTACTS(this, permissionCode)
            }
        }
        imgHomeClear!!.setOnClickListener {
            edtSearch!!.text.clear()
            linearLayout!!.visibility = View.GONE
            searchList.clear()
            if (adapter != null) {
                adapter!!.updateList(searchList)
            }
        }

        edtSearch!!.addTextChangedListener(object : TextWatcher {
            var timer = Timer()
            var DELAY: Long = 1000

            override fun afterTextChanged(p0: Editable?) {
                if (p0!!.length > 0) {
                    timer.cancel()
                    timer = Timer()
                    timer.schedule(object : TimerTask() {
                        override fun run() {
                            getSearchList(p0.toString())
                        }
                    }, DELAY)
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
        })

        imgMyContacts!!.setOnClickListener {
            val intent = Intent(this, MyContactsActivity::class.java)
            startActivityForResult(intent, UPDATE_PROFILE_REQUEST_CODE)
        }

        imgQRReader!!.setOnClickListener {
            startActivity(Intent(this, QRCodeReaderActivity::class.java))
        }

        btnViewQRCode!!.setOnClickListener {

            if (Permissions.Check_STORAGE(this)) {
                displayQRCodeFromFile()
            } else {
                Permissions.Request_STORAGE(this, Constants.STORAGE_PERMISSION_CODE)
            }
        }
    }

    fun displayQRCodeFromFile() {
        val builder = AlertDialog.Builder(this)
        val layoutInflater = layoutInflater
        val dialogView = layoutInflater.inflate(R.layout.layout_qr_code, null)
        builder.setView(dialogView)

        var qrCodeImage = dialogView.findViewById<ImageView>(R.id.rowQRImage)
        var btnCancel = dialogView.findViewById<ImageView>(R.id.btnCancel)
        val alertDialog: AlertDialog = builder.create()
        alertDialog.show()
        alertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        var imagePath = FileOperationsHelper.getQRCodeImage()
        if (imagePath != null) {
            var myBitmap = BitmapFactory.decodeFile(imagePath)
            qrCodeImage.setImageBitmap(myBitmap)
        } else {
            alertDialog.dismiss()
            Toast.makeText(this, "QR Code not available...", Toast.LENGTH_SHORT).show()
        }

        btnCancel.setOnClickListener {
            alertDialog.dismiss()
        }
    }

    fun getSearchList(searchString: String) {
        try {
            this@HomeActivity.runOnUiThread {
                linearLayout!!.visibility = View.GONE
                Log.e("TAG", "Message $searchString")
                if (Utilities.isEmpty(searchString)) {
                    searchList.clear()
                    if (adapter != null)
                        adapter!!.updateList(searchList)
                    return@runOnUiThread
                }
                searchList.clear()
                searchList.addAll(contactViewModel!!.getSearchContactResult(searchString))
                if (!Utilities.isListEmpty(searchList)) {
                    adapter = CustomListAdapter(
                        this,
                        recyclerSearchList,
                        searchList,
                        R.layout.row_my_contacts,
                        object : CustomListAdapter.ItemClickedListener {
                            override fun onItemClicked(
                                view: View?,
                                `object`: Any?,
                                position: Int
                            ) {
                                var model = `object` as ContactModel
                                var bottomSheetDialog: BottomSheetFragment? = null
                                val bundle = Bundle()
                                bundle.putSerializable("contactDetails", model)
                                bottomSheetDialog = BottomSheetFragment.newInstance()
                                bottomSheetDialog.show(
                                    supportFragmentManager,
                                    "Custom Bottom Sheet"
                                )
                                bottomSheetDialog.arguments = bundle
                            }

                            override fun onViewBound(
                                view: View?,
                                `object`: Any?,
                                position: Int
                            ) {
                                var model = `object` as ContactModel
                                var tvName = view!!.findViewById<TextView>(R.id.tvRowMyContactName)
                                var tvNumber =
                                    view.findViewById<TextView>(R.id.tvRowMyContactNumber)
                                val imgPhoto =
                                    view.findViewById<ImageView>(R.id.imgRomMyContactPhoto)
                                tvName.text = model.contactName
                                tvNumber.text = model.mobileNumber

                                if (Utilities.isEmpty(model.photo)) {
                                    imgPhoto.setImageResource(R.drawable.user)
                                } else {
                                    Log.e("Bitmap", "Data ${model.photo}")
                                    val uri = model.photo.toUri()
                                    imgPhoto.setImageURI(uri)
                                }
                            }
                        })

                    recyclerSearchList.adapter = adapter

                } else {
                    // IF CONTACT IS NOT PRESENT IN PHONE DIRECTORY THEN SHOW BUTTON TO SHARE VCARD
                    if (searchString.length > 9) {
                        linearLayout!!.visibility = View.VISIBLE
                        btnShare!!.setOnClickListener {
                            var myContact = ContactModel()
                            myContact.mobileNumber = searchString
                            var dialog: BottomSheetFragment? = null
                            val bundle = Bundle()
                            bundle.putSerializable("contactDetails", myContact)
                            dialog = BottomSheetFragment.newInstance()
                            dialog.show(
                                supportFragmentManager,
                                "Custom Bottom Sheet"
                            )
                            dialog.arguments = bundle
                        }
                    } else {
                        linearLayout!!.visibility = View.GONE
                    }

                    Toast.makeText(this, "Contact Not available", Toast.LENGTH_SHORT)
                        .show()
                }
            }
        } catch (e: Exception) {

            e.printStackTrace()
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == permissionCode) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (Utilities.isListEmpty(locatContactList)) {
                    contactViewModel!!.getContactFromProvider().execute()
                }
            }
        } else if (requestCode == Constants.STORAGE_PERMISSION_CODE) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                displayQRCodeFromFile()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == UPDATE_PROFILE_REQUEST_CODE) {
            contactViewModel!!.getUserDetails()
        }
    }
}
