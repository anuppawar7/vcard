package com.etonius.vcard.activities

import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.provider.MediaStore
import androidx.appcompat.app.AppCompatActivity
import com.axelbuzz.malisamajmatrimony.utils.Utilities
import com.etonius.vcard.R
import com.google.firebase.ml.vision.FirebaseVision
import com.google.firebase.ml.vision.common.FirebaseVisionImage
import com.google.firebase.ml.vision.text.FirebaseVisionText
import com.google.firebase.ml.vision.text.FirebaseVisionTextRecognizer
import kotlinx.android.synthetic.main.activity_scan_visiting_card.*


class ScanVisitingCardActivity : AppCompatActivity() {
    val REQUEST_IMAGE_CAPTURE = 123
    var bitmap: Bitmap? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_scan_visiting_card)

        btnCaptureImage.setOnClickListener {
            if (bitmap != null) {
                bitmap!!.recycle()
                bitmap = null
            }
            tvScanText.text = ""
            takePhoto()
        }

        btnFetchText.setOnClickListener {
            detectText()
        }
    }

    private fun detectText() {
        var firebaseVisionImage = FirebaseVisionImage.fromBitmap(bitmap!!)
        val detector: FirebaseVisionTextRecognizer =
            FirebaseVision.getInstance().onDeviceTextRecognizer
        detector.processImage(firebaseVisionImage)
            .addOnSuccessListener { firebaseVisionText ->
                processTxt(firebaseVisionText)
            }.addOnFailureListener { exception: Exception ->
                exception.localizedMessage
            }
    }

    private fun processTxt(firebaseVisionText: FirebaseVisionText?) {
        val blocks = firebaseVisionText!!.textBlocks
        if (Utilities.isListEmpty(blocks)) {
            return
        }
        for (i in blocks.indices) {
            val lines = blocks[i].lines

            for (j in lines.indices) {
                val element = lines[j].elements
                for (k in element) {
                    tvScanText.textSize = 20.0F
                    tvScanText.append(k.text)
                }
            }

        }
    }

    private fun takePhoto() {
        var cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        if (cameraIntent.resolveActivity(this.packageManager) != null) {
            startActivityForResult(cameraIntent, REQUEST_IMAGE_CAPTURE)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            val bundle = data!!.extras
            bitmap = bundle!!.get("data") as Bitmap
            captureImage.setImageBitmap(bitmap)
        }
    }

}
