package com.etonius.vcard.activities

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.telephony.SmsManager
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.core.net.toUri
import com.axelbuzz.malisamajmatrimony.utils.Utilities
import com.etonius.vcard.R
import com.etonius.vcard.models.ContactModel

class ContactDetailsActivity : DetailsBaseActivity() {

    var tvName: TextView? = null
    var tvNumber: TextView? = null
    var tvEmail: TextView? = null
    var imgProfile: ImageView? = null
    var imgCall: ImageView? = null
    var imgMessage: ImageView? = null
    var imgEmail: ImageView? = null
    var imgWhatsapp: ImageView? = null
    var btnWhatsBusiness: ImageView? = null
    var CALL_PERMISSION_CODE = 111
    var PHONE_STATE_PERMISSION_CODE = 222
    var contactModel = ContactModel()
    var WHATSAPP_BUSINESS_PACKAGE = "com.whatsapp.w4b"
    var WHATSAPP_PACKAGE = "com.whatsapp"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_contact_details)
        setTitle("Profile Details")
        tvName = findViewById(R.id.tvContactDetailsName)
        tvNumber = findViewById(R.id.tvContactDetailsNumber)
        tvEmail = findViewById(R.id.tvContactDetailsEmail)
        imgProfile = findViewById(R.id.imgContactDetailsPhoto)
        imgCall = findViewById(R.id.imgContactDetailsCall)
        imgMessage = findViewById(R.id.imgContactDetailsMessage)
        imgEmail = findViewById(R.id.imgContactDetailsEmail)
        imgWhatsapp = findViewById(R.id.imgContactDetailsWhatsUp)
        btnWhatsBusiness = findViewById(R.id.imgContactDetailsWhatsBusiness)


        var whatsBusinessApp = appInstalledOrNot(WHATSAPP_BUSINESS_PACKAGE)
        var whatsApp = appInstalledOrNot(WHATSAPP_PACKAGE)
        if (!whatsBusinessApp) {
            btnWhatsBusiness!!.visibility = View.GONE
        }

        if (!whatsApp) {
            imgWhatsapp!!.visibility = View.GONE
        }

        contactModel = intent.getSerializableExtra("contactDetails") as ContactModel
        tvName!!.text = contactModel.contactName
        tvNumber!!.text = contactModel.mobileNumber
        if (Utilities.isEmpty(contactModel.emailId)) {
            tvEmail!!.visibility = View.GONE
        } else {
            tvEmail!!.visibility = View.VISIBLE
            tvEmail!!.text = contactModel.emailId
        }
        if (Utilities.isEmpty(contactModel.photo)) {
            imgProfile!!.setImageResource(R.drawable.user)
        } else {
            var displayImagePath = contactModel.photo.replace("photo", "display_photo")
            val uri = displayImagePath.toUri()
            imgProfile!!.setImageURI(uri)
        }

        imgCall!!.setOnClickListener {
            if (hasPermission(this, Manifest.permission.CALL_PHONE)) {
                makeCall(contactModel.mobileNumber)
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(
                        arrayOf(Manifest.permission.CALL_PHONE),
                        CALL_PERMISSION_CODE
                    )
                }
            }
        }

        imgMessage!!.setOnClickListener {
            if (hasPermission(this, Manifest.permission.READ_PHONE_STATE)) {
                displayMSGConfirmationDialog(contactModel)
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    this.requestPermissions(
                        arrayOf(
                            Manifest.permission.READ_PHONE_STATE,
                            Manifest.permission.SEND_SMS
                        ), PHONE_STATE_PERMISSION_CODE
                    )
                }
            }
        }

        imgEmail!!.setOnClickListener {
            sendEmail(contactModel)
        }

        imgWhatsapp!!.setOnClickListener {
            val isAppInstalled = appInstalledOrNot(WHATSAPP_PACKAGE)
            if (isAppInstalled) {
                if (Utilities.isEmpty(contactModel.contactName)) {
                    sendWhatsAppMessage(contactModel.mobileNumber, "")
                } else {
                    sendWhatsAppMessage(contactModel.mobileNumber, contactModel.contactName)
                }
            } else {
                Toast.makeText(context, "Whatsapp not available...", Toast.LENGTH_SHORT).show()
            }
        }

        btnWhatsBusiness!!.setOnClickListener {
            val isAppInstalled = appInstalledOrNot(WHATSAPP_BUSINESS_PACKAGE)
            if (isAppInstalled) {
                if (Utilities.isEmpty(contactModel.contactName)) {
                    sendWhatsAppMessage(contactModel.mobileNumber, "")
                } else {
                    sendWhatsAppMessage(contactModel.mobileNumber, contactModel.contactName)
                }
            } else {
                Toast.makeText(context, "Whatsapp not available...", Toast.LENGTH_SHORT).show()
            }
        }

    }

    private fun sendWhatsAppMessage(mobileNumber: String, contactName: String) {
        try {
            val text: String
            if (Utilities.isEmpty(contactName)) {
                text = "Hello ,\n" +
                        "Here is the Digital Business VCard for Hi Tech Enterprises\n" +
                        "www.etonius.com/aboutus"
            } else {
                text = "Hello ${contactModel.contactName},\n" +
                        "Here is the Digital Business VCard for Hi Tech Enterprises\n" +
                        "www.etonius.com/aboutus"
            }

            val toNumber: String?
            toNumber = mobileNumber.replace("+", "").replace(" ", "")
            val mNumber: String?
            mNumber = if (toNumber.length == 10) {
                "91$toNumber"
            } else {
                toNumber
            }
            val intent = Intent(Intent.ACTION_VIEW)
            intent.data = Uri.parse("http://api.whatsapp.com/send?phone=$mNumber&text=$text")
            startActivity(intent)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun makeCall(mobileNumber: String) {
        val intent = Intent(Intent.ACTION_CALL, Uri.parse("tel:$mobileNumber"))
        startActivity(intent!!)
    }

    private fun displayMSGConfirmationDialog(contactModel: ContactModel) {
        val builder = AlertDialog.Builder(this)
        if (Utilities.isEmpty(contactModel.contactName)) {
            builder.setTitle("Message send to : ${contactModel.mobileNumber}")
        } else {
            builder.setTitle("Message send to : ${contactModel.contactName}")
        }
        val layoutInflater = layoutInflater
        val dialogView = layoutInflater!!.inflate(R.layout.row_confirm_sms_layout, null)
        val edtMessage = dialogView.findViewById<EditText>(R.id.edtRowSMSText)
        builder.setView(dialogView)
        if (!Utilities.isEmpty(contactModel.contactName)) {
            edtMessage.setText("Hello ${contactModel.contactName},\nHere is the Digital Business VCard for Hi Tech Enterprises\n www.etonius.com/aboutus")
        }

        try {
            builder.setPositiveButton("Send") { dialogInterface, which ->
                val message = Utilities.extractDataFromEditText(edtMessage)
                val smsManager = SmsManager.getDefault() as SmsManager
                smsManager.sendTextMessage(
                    contactModel.mobileNumber,
                    null,
                    "" + message,
                    null,
                    null
                )
                dialogInterface.dismiss()
            }
        } catch (e: Exception) {
            e.printStackTrace()
            Log.e("Bottom Dialog", "Exception ${e.printStackTrace()}")
        }
        val alertDialog: AlertDialog = builder.create()
        alertDialog.show()
    }

    private fun sendEmail(contactModel: ContactModel) {
        val intent = Intent(Intent.ACTION_SEND)
        intent.type = "message/rfc822"
        intent.putExtra(Intent.EXTRA_EMAIL, arrayOf(contactModel.emailId))
        intent.putExtra(Intent.EXTRA_SUBJECT, "Digital VCard Hi Tech Enterprises")
        intent.putExtra(
            Intent.EXTRA_TEXT, "Hello ${contactModel.contactName},\n \n" +
                    "Here is the Digital Business VCard for Hi Tech Enterprises\n" +
                    "www.etonius.com/aboutus"
        )
        startActivity(Intent.createChooser(intent, "Send Email"))
    }

    private fun appInstalledOrNot(uri: String): Boolean {
        val pm = this.packageManager
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES)
            return true
        } catch (e: PackageManager.NameNotFoundException) {
        }
        return false
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            CALL_PERMISSION_CODE -> {
                makeCall(contactModel.mobileNumber)
            }
            PHONE_STATE_PERMISSION_CODE -> {
                displayMSGConfirmationDialog(contactModel)
            }
            else -> super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }

    private fun hasPermission(context: Context, permission: String): Boolean {
        return ContextCompat.checkSelfPermission(
            context,
            permission
        ) == PackageManager.PERMISSION_GRANTED
    }
}
