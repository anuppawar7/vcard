package com.etonius.vcard.fragments

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.telephony.SmsManager
import android.telephony.TelephonyManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.core.content.ContextCompat.getSystemService
import androidx.fragment.app.Fragment
import com.axelbuzz.malisamajmatrimony.utils.Utilities
import com.etonius.vcard.R
import com.etonius.vcard.models.ContactModel
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

/**
 * A simple [Fragment] subclass.
 */
class BottomSheetFragment : BottomSheetDialogFragment() {

    var contactModel = ContactModel()
    var btnCall: ImageView? = null
    var btnMessage: ImageView? = null
    var btnEmail: ImageView? = null
    var btnWhatsUp: ImageView? = null
    var btnWhatsBusiness: ImageView? = null
    var CALL_PERMISSION_CODE = 111
    var PHONE_STATE_PERMISSION_CODE = 222
    var WHATSAPP_BUSINESS_PACKAGE = "com.whatsapp.w4b"
    var WHATSAPP_PACKAGE = "com.whatsapp"

    companion object {
        fun newInstance(): BottomSheetFragment {
            return BottomSheetFragment()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_bottom_sheet, container, false)
        btnCall = view.findViewById(R.id.btnBottomDialogCall)
        btnMessage = view.findViewById(R.id.btnBottomDialogMessage)
        btnEmail = view.findViewById(R.id.btnBottomDialogEmail)
        btnWhatsUp = view.findViewById(R.id.btnBottomDialogWhatsUp)
        btnWhatsBusiness = view.findViewById(R.id.btnBottomDialogWhatsBusiness)

        checkSimNetwork()

        var whatsBusinessApp = appInstalledOrNot(WHATSAPP_BUSINESS_PACKAGE)
        var whatsApp = appInstalledOrNot(WHATSAPP_PACKAGE)
        if (!whatsBusinessApp) {
            btnWhatsBusiness!!.visibility = View.GONE
        }

        if (!whatsApp) {
            btnWhatsUp!!.visibility = View.GONE
        }

        if (arguments != null) {
            contactModel = arguments!!.getSerializable("contactDetails") as ContactModel
            Log.e("Bottom Dialog", "Model === $contactModel")
        }

        btnCall!!.setOnClickListener {
            if (hasPermission(context!!, Manifest.permission.CALL_PHONE)) {
                dismiss()
                makeCall(contactModel.mobileNumber as String)
            } else {
                this.requestPermissions(
                    arrayOf(Manifest.permission.CALL_PHONE),
                    CALL_PERMISSION_CODE
                )
            }
        }

        btnMessage!!.setOnClickListener {
            if (hasPermission(context!!, Manifest.permission.READ_PHONE_STATE)) {
                dismiss()
                displayMSGConfirmationDialog(contactModel)
            } else {
                this.requestPermissions(
                    arrayOf(Manifest.permission.READ_PHONE_STATE, Manifest.permission.SEND_SMS),
                    PHONE_STATE_PERMISSION_CODE
                )
            }
        }

        btnEmail!!.setOnClickListener {
            sendEmail(contactModel)
        }

        btnWhatsUp!!.setOnClickListener {
            val isAppInstalled = appInstalledOrNot(WHATSAPP_PACKAGE)
            if (isAppInstalled) {
                dismiss()
                if (Utilities.isEmpty(contactModel.contactName)) {
                    sendWhatsAppMessage(contactModel.mobileNumber, "")
                } else {
                    sendWhatsAppMessage(contactModel.mobileNumber, contactModel.contactName)
                }
            } else {
                Toast.makeText(context, "Whatsapp not available...", Toast.LENGTH_SHORT).show()
            }
        }

        btnWhatsBusiness!!.setOnClickListener {
            val isAppInstalled = appInstalledOrNot(WHATSAPP_BUSINESS_PACKAGE)
            if (isAppInstalled) {
                dismiss()
                if (Utilities.isEmpty(contactModel.contactName)) {
                    sendWhatsAppMessage(contactModel.mobileNumber, "")
                } else {
                    sendWhatsAppMessage(contactModel.mobileNumber, contactModel.contactName)
                }
            } else {
                Toast.makeText(context, "Whatsapp not available...", Toast.LENGTH_SHORT).show()
            }
        }

        return view
    }

    private fun sendWhatsAppMessage(mobileNumber: String, contactName: String) {
        try {
            val text: String
            if (Utilities.isEmpty(contactName)) {
                text = "Hello ,\n" +
                        "Here is the Digital Business VCard for Hi Tech Enterprises\n" +
                        "www.etonius.com/aboutus"
            } else {
                text = "Hello ${contactModel.contactName},\n" +
                        "Here is the Digital Business VCard for Hi Tech Enterprises\n" +
                        "www.etonius.com/aboutus"
            }
            val toNumber: String?
            toNumber = mobileNumber.replace("+", "").replace(" ", "")
            val mNumber: String?
            mNumber = if (toNumber.length == 10) {
                "91$toNumber"
            } else {
                toNumber
            }
            val intent = Intent(Intent.ACTION_VIEW)
            intent.data = Uri.parse("http://api.whatsapp.com/send?phone=$mNumber&text=$text")
            startActivity(intent)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun makeCall(mobileNumber: String) {
        val intent = Intent(Intent.ACTION_CALL, Uri.parse("tel:$mobileNumber"))
        startActivity(intent)
    }

    private fun displayMSGConfirmationDialog(contactModel: ContactModel) {
        val builder = AlertDialog.Builder(context!!)
        if (Utilities.isEmpty(contactModel.contactName)) {
            builder.setTitle("Message send to : ${contactModel.mobileNumber}")
        } else {
            builder.setTitle("Message send to : ${contactModel.contactName}")
        }
        val layoutInflater = layoutInflater
        val dialogView = layoutInflater!!.inflate(R.layout.row_confirm_sms_layout, null)
        val edtMessage = dialogView.findViewById<EditText>(R.id.edtRowSMSText)
        builder.setView(dialogView)
        if (!Utilities.isEmpty(contactModel.contactName)) {
            edtMessage.setText("Hello ${contactModel.contactName},\nHere is the Digital Business VCard for Hi Tech Enterprises\n www.etonius.com/aboutus")
        }

        try {
            builder.setPositiveButton("Send") { dialogInterface, which ->
                val message = Utilities.extractDataFromEditText(edtMessage)
                val smsManager = SmsManager.getDefault() as SmsManager
                smsManager.sendTextMessage(
                    contactModel.mobileNumber,
                    null,
                    "" + message,
                    null,
                    null
                )
                dialogInterface.dismiss()
            }
        } catch (e: Exception) {
            e.printStackTrace()
            Log.e("Bottom Dialog", "Exception ${e.printStackTrace()}")
        }
        val alertDialog: AlertDialog = builder.create()
        alertDialog.show()
    }

    private fun sendEmail(contactModel: ContactModel) {
        dismiss()
        val intent = Intent(Intent.ACTION_SEND)
        intent.type = "message/rfc822"
        intent.putExtra(Intent.EXTRA_EMAIL, arrayOf(contactModel.emailId))
        intent.putExtra(Intent.EXTRA_SUBJECT, "Digital VCard Hi Tech Enterprises")
        intent.putExtra(
            Intent.EXTRA_TEXT, "Hello ${contactModel.contactName},\n \n" +
                    "Here is the Digital Business VCard for Hi Tech Enterprises\n" +
                    "www.etonius.com/aboutus"
        )
        startActivity(Intent.createChooser(intent, "Send Email"))

    }

    private fun appInstalledOrNot(uri: String): Boolean {
        val pm = context!!.packageManager
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES)
            return true
        } catch (e: PackageManager.NameNotFoundException) {
        }
        return false
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == CALL_PERMISSION_CODE) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                dismiss()
                makeCall(contactModel.mobileNumber)
            }
        } else if (requestCode == PHONE_STATE_PERMISSION_CODE) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                dismiss()
                displayMSGConfirmationDialog(contactModel)
            }
        }
    }

    private fun hasPermission(context: Context, permission: String): Boolean {
        return ContextCompat.checkSelfPermission(
            context,
            permission
        ) == PackageManager.PERMISSION_GRANTED
    }

    private fun checkSimNetwork() {
        val telephoneManager =
            context!!.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
        var simState = telephoneManager.simState
        Log.e("Fragment", "Sim state $simState")
    }
}
