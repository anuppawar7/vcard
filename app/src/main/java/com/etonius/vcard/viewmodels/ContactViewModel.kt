package com.etonius.vcard.viewmodels

import android.app.Application
import android.content.Context
import android.os.AsyncTask
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.AndroidViewModel
import com.etonius.vcard.helpers.ContactHelper
import com.etonius.vcard.helpers.DatabaseHelper
import com.etonius.vcard.helpers.QRCodeHelper
import com.etonius.vcard.models.ContactModel
import com.etonius.vcard.utils.Constants
import java.io.InputStream
import java.net.HttpURLConnection
import java.net.URL

class ContactViewModel : AndroidViewModel {
    var context: Context? = null
    var contactList = ArrayList<ContactModel>()
    var databaseHelper: DatabaseHelper? = null
    var TAG = ContactViewModel::class.java.name

    constructor(application: Application) : super(application) {
        this.context = application.applicationContext
        databaseHelper = DatabaseHelper.getInstance(context)
    }

    inner class getContactFromProvider :
        AsyncTask<Void, Void, ArrayList<ContactModel>>() {

        override fun onPreExecute() {
            super.onPreExecute()
        }

        override fun doInBackground(vararg p0: Void?): ArrayList<ContactModel>? {
            return ContactHelper.loadMyContactList(context!!)
        }

        override fun onPostExecute(result: ArrayList<ContactModel>?) {
            super.onPostExecute(result)
            //  progressBar!!.visibility = View.GONE
            Log.e("TAG", "ContactProviderList $result")
            contactList = result!!
            insertContactAsyncTask().execute(contactList)
        }
    }

    private inner class insertContactAsyncTask :
        AsyncTask<ArrayList<ContactModel>, Void, Void>() {

        override fun doInBackground(vararg notes: ArrayList<ContactModel>): Void? {
            Log.e("TAG", "InsertAsync === ${notes[0]}")
            databaseHelper!!.insertContact(notes[0])
            return null
        }

        override fun onPostExecute(result: Void?) {
            super.onPostExecute(result)
            Toast.makeText(context, "Inserted successfully...", Toast.LENGTH_SHORT).show()
        }
    }

    fun getContactFromLocal(): ArrayList<ContactModel> {
        return getContactAsyncTask().execute().get()
    }

    inner class getContactAsyncTask :
        AsyncTask<Void, Void, ArrayList<ContactModel>>() {
        override fun doInBackground(vararg p0: Void?): ArrayList<ContactModel>? {
            var list = databaseHelper!!.contactList
            return list
        }

        override fun onPostExecute(result: ArrayList<ContactModel>?) {
            super.onPostExecute(result)
            Log.e(TAG, "DB contactList 1== $result")
            Log.e(TAG, "DB contactList Size== ${result!!.size} ")
        }
    }

    fun getUserDetails(): ContactModel {
        return databaseHelper!!.userTableId()
    }

    fun deleteContacts() {
        databaseHelper!!.deleteAllData()
    }

    fun getSearchContactResult(searchString: String): List<ContactModel> {
        return databaseHelper!!.searchContactList(searchString)
    }

    fun updateUserDetails(contactModel: ContactModel) {
        databaseHelper!!.updateUser(contactModel)
    }

    inner class generateQRCode(var userModel: ContactModel) :
        AsyncTask<Void, Void, InputStream>() {

        var name = userModel.contactName
        var businessName = userModel.companyName
        var address = userModel.address
        var contactNo = userModel.mobileNumber

        var qrCodeData = name + "|" + businessName + "|" + address + "|" + contactNo

        override fun doInBackground(vararg p0: Void?): InputStream {
            var inputStream: InputStream? = null
            try {
                inputStream = null
                var url = URL(Constants.GOOGLE_CHART_API_URL + Constants.QR_API_URL + qrCodeData)
                var urlConnection = url.openConnection()

                var httpConn = urlConnection as HttpURLConnection
                httpConn.requestMethod = "GET"
                httpConn.connect()

                if (httpConn.responseCode == HttpURLConnection.HTTP_OK) {
                    inputStream = httpConn.inputStream
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
            return inputStream!!
        }

        override fun onPostExecute(result: InputStream?) {
            super.onPostExecute(result)
            QRCodeHelper.saveQRToFile(context!!, result!!, userModel.contactName)
        }
    }

}