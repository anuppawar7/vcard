package com.etonius.vcard.helpers

import android.content.ContentResolver
import android.content.ContentUris
import android.content.Context
import android.net.Uri
import android.provider.ContactsContract
import android.util.Log
import com.etonius.vcard.models.ContactModel
import java.util.*
import kotlin.collections.ArrayList


class ContactHelper {

    companion object {
        val TAG = "Contact Provider Helper"
        fun loadMyContactList(context: Context): ArrayList<ContactModel> {
            var contactList = ArrayList<ContactModel>()
            val uri = ContactsContract.Contacts.CONTENT_URI
            val resolver: ContentResolver = context.contentResolver
            val contactsCursor = resolver.query(
                uri,
                null, null, null, ContactsContract.Contacts.DISPLAY_NAME + " DESC "
            )

            try {
                if (contactsCursor!!.moveToFirst()) {
                    do {
                        var myContact = ContactModel()
                        val id =
                            contactsCursor.getString(contactsCursor.getColumnIndex(ContactsContract.Contacts._ID))
                        myContact.contactId = id
                        val name =
                            contactsCursor.getString(contactsCursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME))
                        myContact.contactName = name

                        val phoneNumber = (contactsCursor.getString(
                            contactsCursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER)
                        )).toInt()

                        if (phoneNumber > 0) {
                            val cursorPhone = context.contentResolver.query(
                                ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                                null,
                                ContactsContract.CommonDataKinds.Phone.CONTACT_ID + "=?",
                                arrayOf(id),
                                null
                            )
                            if (cursorPhone!!.count > 0) {
                                if (cursorPhone.moveToNext()) {
                                    val mobNumber = cursorPhone.getString(
                                        cursorPhone.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)
                                    )
                                    myContact.mobileNumber = mobNumber
                                }
                            }
                            cursorPhone.close()
                            val cursorEmail = context.contentResolver.query(
                                ContactsContract.CommonDataKinds.Email.CONTENT_URI,
                                null,
                                ContactsContract.CommonDataKinds.Email.CONTACT_ID + "=?",
                                arrayOf(id),
                                null
                            )
                            if (cursorEmail!!.count > 0) {
                                if (cursorEmail.moveToNext()) {
                                    val emailId = cursorEmail.getString(
                                        cursorEmail.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA)
                                    )
                                    myContact.emailId = emailId
                                    Log.e(TAG, "Email Id $emailId ")
                                }
                            } else {
                                myContact.emailId = ""
                            }
                            cursorEmail.close()

                            try {
                                val cursorAddress = context.contentResolver.query(
                                    ContactsContract.CommonDataKinds.StructuredPostal.CONTENT_URI,
                                    null,
                                    ContactsContract.CommonDataKinds.SipAddress.CONTACT_ID + "=?",
                                    arrayOf(id),
                                    null
                                )
                                if (cursorAddress!!.count > 0) {
                                    if (cursorAddress.moveToNext()) {
                                        var street = getAddressString(
                                            cursorAddress.getString(
                                                cursorAddress.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.STREET)
                                            )
                                        )
                                        var city = getAddressString(
                                            cursorAddress.getString(
                                                cursorAddress.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.CITY)
                                            )
                                        )
                                        var country = getAddressString(
                                            cursorAddress.getString(
                                                cursorAddress.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.COUNTRY)
                                            )
                                        )

                                        myContact.address = street + city + country
                                    }
                                }
                                cursorAddress.close()
                            } catch (e: Exception) {
                                e.printStackTrace()
                            }

                            var orgWhereParam = arrayOf(
                                id,
                                ContactsContract.CommonDataKinds.Organization.CONTENT_ITEM_TYPE
                            )

                            var cursorOrganization = context.contentResolver.query(
                                ContactsContract.Data.CONTENT_URI,
                                null,
                                ContactsContract.CommonDataKinds.Organization.CONTACT_ID + " = ? AND " + ContactsContract.Data.MIMETYPE + " = ?",
                                orgWhereParam,
                                null
                            )
                            if (cursorOrganization!!.count > 0) {
                                if (cursorOrganization.moveToNext()) {
                                    var orgName = cursorOrganization.getString(
                                        cursorOrganization.getColumnIndex(ContactsContract.CommonDataKinds.Organization.DATA)
                                    )
                                    var title = cursorOrganization.getString(
                                        cursorOrganization.getColumnIndex(ContactsContract.CommonDataKinds.Organization.TITLE)
                                    )
                                    myContact.companyName = orgName
                                }
                            }
                            cursorOrganization.close()
                            val photoUri = ContentUris.withAppendedId(
                                ContactsContract.Contacts.CONTENT_URI,
                                id.toLong()
                            )
                            val inputStream = ContactsContract.Contacts.openContactPhotoInputStream(
                                context.contentResolver,
                                photoUri
                            )

                            if (inputStream == null) {

                            } else {
                                val photoPath = Uri.withAppendedPath(
                                    photoUri,
                                    ContactsContract.Contacts.Photo.CONTENT_DIRECTORY
                                )
                                /*val photoPath = Uri.withAppendedPath(
                                    photoUri,
                                    ContactsContract.Contacts.Photo.DISPLAY_PHOTO
                                )*/
                                Log.e(TAG, "Photo $photoPath")
                                if (photoPath != null)
                                    myContact.photo = photoPath.toString()
                            }
                            contactList.add(myContact)
                        }

                    } while (contactsCursor.moveToNext())
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
            contactsCursor!!.close()
            Log.e(TAG, "Phone Number " + contactList)
            Log.e(TAG, "List Size " + contactList.size)
            return removeDuplicate(contactList)
        }

        private fun getAddressString(string: String?): String {
            if (string != null) {
                return string
            }
            return ""
        }

        private fun removeDuplicate(list: ArrayList<ContactModel>): ArrayList<ContactModel> {
            var newList = ArrayList<ContactModel>()
            try {
                var set = TreeSet(object : Comparator<ContactModel> {
                    override fun compare(p0: ContactModel?, p1: ContactModel?): Int {
                        if (p0!!.mobileNumber!!.equals(p1!!.mobileNumber, false)) {
                            return 0
                        }
                        if (p0.contactName!!.equals(p1.contactName, false)) {
                            return 0
                        }
                        return -1
                    }
                })
                set.addAll(list)
                newList = ArrayList(set)

            } catch (e: Exception) {
                e.printStackTrace()
            }
            return newList
        }
    }

    fun joinString(list: List<String>): String {
        var stringBuilder = StringBuffer()
        var joinString = ""
        for (item in list) {
            stringBuilder.append(joinString)
            stringBuilder.append(item)
            joinString = ","

        }
        return joinString
    }
}

