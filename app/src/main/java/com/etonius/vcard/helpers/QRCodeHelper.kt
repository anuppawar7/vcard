package com.etonius.vcard.helpers

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Environment
import android.provider.MediaStore
import android.widget.Toast
import java.io.File
import java.io.FileOutputStream
import java.io.InputStream
import java.io.OutputStream

class QRCodeHelper {

    companion object {

        fun saveQRToFile(
            context: Context,
            inputStream: InputStream,
            userName: String
        ) {

            try {
                var bitmap = BitmapFactory.decodeStream(inputStream)
                inputStream.close()
                var folderPath =
                    File("" + Environment.getExternalStorageDirectory() + File.separator + "VCard/QRCodes")
                if (!folderPath.exists()) {
                    folderPath.mkdirs()
                }

                if (folderPath.isDirectory) {
                    val children = folderPath.list()
                    if (children != null && children.isNotEmpty()) {
                        for (i in children.indices) {
                            File(folderPath, children[i]).delete()
                        }
                    }
                }

                var outPutStream: OutputStream? = null
                var file = File(folderPath, "QRCode_${userName.trim()}.jpg")
                outPutStream = FileOutputStream(file)

                var pictureBitmap = bitmap
                pictureBitmap!!.compress(Bitmap.CompressFormat.JPEG, 100, outPutStream)
                outPutStream.flush()
                outPutStream.close()

                var store = MediaStore.Images.Media.insertImage(
                    context.contentResolver,
                    file.absolutePath,
                    file.name,
                    file.name
                )
                if (!store.isNullOrEmpty()) {
                    Toast.makeText(context, "QR Code store successfully..", Toast.LENGTH_SHORT)
                        .show()
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

}