package com.etonius.vcard.helpers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import androidx.annotation.Nullable;

import com.axelbuzz.malisamajmatrimony.utils.Utilities;
import com.etonius.vcard.models.ContactModel;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Abhijeet
 */

public class DatabaseHelper extends SQLiteOpenHelper {
    private SQLiteDatabase database;
    private static DatabaseHelper databaseHelper;
    private static final String DATABASE_NAME = "MyContact";
    private static final int DATABASE_VERSION = 1;

    private DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        createSchemas();
    }

    private static String DATATYPE_TEXT = " text";
    private static String DATATYPE_INT = " integer";

    public final class ContactDetailsTable {
        public static final String TABLE_NAME = "ContactDetails";
        public static final String ContactId = "ContactId";
        public static final String ContactDisplayName = "ContactDisplayName";
        public static final String ContactNumber = "ContactNumber";
        public static final String ContactEmail = "ContactEmail";
        public static final String ContactPhoto = "ContactPhoto";
        public static final String CompanyName = "CompanyName";
        public static final String Address = "Address";
        public static final String CompanyLogo = "CompanyLogo";
    }

    public final class UserDetailsTable {
        public static final String TABLE_NAME = "User";
        public static final String UserId = "UserId";
        public static final String UserName = "UserName";
        public static final String UserNumber = "UserNumber";
        public static final String UserEmailId = "UserEmailId";
        public static final String CompanyName = "CompanyName";
        public static final String UserAddress = "UserAddress";
        public static final String CompanyLogo = "CompanyLogo";
        public static final String MyProfile = "MyProfile";
    }

    private final String CREATE_CONTACT_DETAILS_TABLE = "CREATE TABLE IF NOT EXISTS " + ContactDetailsTable.TABLE_NAME
            + " (" + createColumns(ContactDetailsTable.ContactId, ContactDetailsTable.ContactDisplayName, ContactDetailsTable.ContactNumber, ContactDetailsTable.ContactEmail, ContactDetailsTable.Address, ContactDetailsTable.CompanyName, ContactDetailsTable.ContactPhoto, ContactDetailsTable.CompanyLogo) + ");";

    private final String CREATE_USER_DETAILS_TABLE = "CREATE TABLE IF NOT EXISTS " + UserDetailsTable.TABLE_NAME
            + " (" + createColumns(UserDetailsTable.UserId, UserDetailsTable.UserName, UserDetailsTable.UserNumber, UserDetailsTable.UserEmailId, UserDetailsTable.CompanyName, UserDetailsTable.UserAddress, UserDetailsTable.CompanyLogo, UserDetailsTable.MyProfile) + ");";


    public static DatabaseHelper getInstance(Context context) {
        if (databaseHelper == null) {
            databaseHelper = new DatabaseHelper(context);
        }
        return databaseHelper;
    }

    private void createSchemas() {
        if (database == null)
            database = getWritableDatabase();

        database.execSQL(CREATE_CONTACT_DETAILS_TABLE);
        database.execSQL(CREATE_USER_DETAILS_TABLE);
    }

    // Method is called during creation of the database
    @Override
    public void onCreate(SQLiteDatabase database) {
//        checkForDatabase();
    }

    // Method is called during an upgrade of the database,
    @Override
    public void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {
        Log.w(DatabaseHelper.class.getName(),
                "Upgrading database from version " + oldVersion + " to "
                        + newVersion + ", which will destroy all old data");
        database.execSQL("DROP TABLE IF EXISTS MyTest");
        onCreate(database);
    }

    private String createColumns(String... columnNames) {
        StringBuilder columns = new StringBuilder();
        for (int i = 0; i < columnNames.length; i++) {
            columns.append(columnNames[i]);
            columns.append(" text");
            if (columnNames.length > 1 && i < columnNames.length - 1) {
                columns.append(",");
            }
        }
        return columns.toString();
    }

    public void insertContact(List<ContactModel> contactList) {
        if (Utilities.Companion.isListEmpty(contactList))
            return;
        try {
            if (contactList != null && contactList.size() > 0)
                database.delete(ContactDetailsTable.TABLE_NAME, null, null);
            database.beginTransaction();
            for (ContactModel contactModel : contactList) {
                ContentValues values = new ContentValues();
                values.put(ContactDetailsTable.ContactId, "" + contactModel.getContactId());
                values.put(ContactDetailsTable.ContactDisplayName, "" + contactModel.getContactName());
                values.put(ContactDetailsTable.ContactNumber, "" + contactModel.getMobileNumber());
                values.put(ContactDetailsTable.ContactEmail, "" + contactModel.getEmailId());
                values.put(ContactDetailsTable.ContactPhoto, "" + contactModel.getPhoto());
                values.put(ContactDetailsTable.Address, "" + contactModel.getAddress());
                values.put(ContactDetailsTable.CompanyName, "" + contactModel.getCompanyName());
                values.put(ContactDetailsTable.CompanyLogo, "" + contactModel.getCompanyLogo());
                database.insert(ContactDetailsTable.TABLE_NAME, null, values);
            }
            database.setTransactionSuccessful();
            database.endTransaction();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Nullable
    public ArrayList<ContactModel> getContactList() {
        ArrayList<ContactModel> contactModels = new ArrayList<>();
        try {
            Cursor cursor = database.query(ContactDetailsTable.TABLE_NAME, null, null, null, null, null, null);
            if (cursor == null || cursor.getCount() == 0)
                return contactModels;
            cursor.moveToFirst();
            do {
                ContactModel contactModel = new ContactModel();
                contactModel.setContactId(cursor.getString(0));
                contactModel.setContactName(cursor.getString(1));
                contactModel.setMobileNumber(cursor.getString(2));
                contactModel.setEmailId(cursor.getString(3));
                contactModel.setPhoto(cursor.getString(4));
                contactModels.add(contactModel);
            } while (cursor.moveToNext());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return contactModels;
    }

    public void updateUser(ContactModel contactModel) {
        try {
            database.beginTransaction();
            ContentValues values = new ContentValues();
            values.put(UserDetailsTable.UserId, "" + contactModel.getContactId());
            values.put(UserDetailsTable.UserName, "" + contactModel.getContactName());
            values.put(UserDetailsTable.UserNumber, "" + contactModel.getMobileNumber());
            values.put(UserDetailsTable.UserEmailId, "" + contactModel.getEmailId());
            values.put(UserDetailsTable.CompanyName, "" + contactModel.getCompanyName());
            values.put(UserDetailsTable.UserAddress, "" + contactModel.getAddress());
            values.put(UserDetailsTable.CompanyLogo, "" + contactModel.getCompanyLogo());
            values.put(UserDetailsTable.MyProfile, "" + contactModel.getPhoto());
            ContactModel model = userTableId();
            if (!Utilities.Companion.isEmpty(model.getContactId())) {
                database.update(UserDetailsTable.TABLE_NAME, values, UserDetailsTable.UserId + "=?", new String[]{model.getContactId()});
            } else {
                database.insert(UserDetailsTable.TABLE_NAME, null, values);
            }
            database.setTransactionSuccessful();
            database.endTransaction();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ContactModel userTableId() {
        ContactModel contactModel = new ContactModel();
        Cursor cursor = database.query(UserDetailsTable.TABLE_NAME, null, null, null, null, null, null, String.valueOf(1));
        if (cursor.getCount() > 0) {
            if (cursor.moveToFirst()) {
                contactModel.setContactId(cursor.getString(cursor.getColumnIndex(UserDetailsTable.UserId)));
                contactModel.setContactName(cursor.getString(cursor.getColumnIndex(UserDetailsTable.UserName)));
                contactModel.setEmailId(cursor.getString(cursor.getColumnIndex(UserDetailsTable.UserEmailId)));
                contactModel.setMobileNumber(cursor.getString(cursor.getColumnIndex(UserDetailsTable.UserNumber)));
                contactModel.setAddress(cursor.getString(cursor.getColumnIndex(UserDetailsTable.UserAddress)));
                contactModel.setCompanyName(cursor.getString(cursor.getColumnIndex(UserDetailsTable.CompanyName)));
                contactModel.setCompanyLogo(cursor.getString(cursor.getColumnIndex(UserDetailsTable.CompanyLogo)));
                contactModel.setPhoto(cursor.getString(cursor.getColumnIndex(UserDetailsTable.MyProfile)));
            }
        }
        cursor.close();
        return contactModel;
    }

    @NotNull
    public List<ContactModel> searchContactList(@NotNull String serachString) {
        List<ContactModel> searchContactList = new ArrayList<>();
        try {
            String query = null;
            if (Utilities.Companion.isEmpty(serachString)) {
                query = "SELECT * FROM " + ContactDetailsTable.TABLE_NAME;
            } else {
                query = "SELECT * FROM " + ContactDetailsTable.TABLE_NAME + " WHERE " + ContactDetailsTable.ContactDisplayName + " LIKE '%" + serachString + "%' OR " + ContactDetailsTable.ContactNumber + " LIKE '%" + serachString + "%'";
            }
            Cursor cursor = database.rawQuery(query, null);
            if (cursor == null || cursor.getCount() == 0)
                return searchContactList;
            cursor.moveToFirst();
            do {
                ContactModel contactModel = new ContactModel();
                contactModel.setContactId(cursor.getString(cursor.getColumnIndex(ContactDetailsTable.ContactId)));
                contactModel.setContactName(cursor.getString(cursor.getColumnIndex(ContactDetailsTable.ContactDisplayName)));
                contactModel.setMobileNumber(cursor.getString(cursor.getColumnIndex(ContactDetailsTable.ContactNumber)));
                contactModel.setEmailId(cursor.getString(cursor.getColumnIndex(ContactDetailsTable.ContactEmail)));
                contactModel.setPhoto(cursor.getString(cursor.getColumnIndex(ContactDetailsTable.ContactPhoto)));
                searchContactList.add(contactModel);
            } while (cursor.moveToNext());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return searchContactList;
    }

    /*public void upadteAnswer(QuestionModel mQuestionModel) {
        try {

            ContentValues values = new ContentValues();
            values.put(QuestionAnswer.QuestionID, "" + mQuestionModel.getQuestionId());
            values.put(QuestionAnswer.Question, "" + mQuestionModel.getQuestion());
            values.put(QuestionAnswer.AnswerId, "" + mQuestionModel.getAnswerId());

            String strSQL = " UPDATE " + QuestionAnswer.TABLE_NAME + " SET " + QuestionAnswer.AnswerId + " = "
                    + mQuestionModel.getAnswerId() + " WHERE " + QuestionAnswer.QuestionID + " = " + mQuestionModel.getQuestionId();

            database.execSQL(strSQL);
            database.setTransactionSuccessful();
            database.endTransaction();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public QuestionModel getQuetionModel(@NotNull String questionId) {
        QuestionModel questionModel = new QuestionModel();

        String query = "Select * from " + QuestionAnswer.TABLE_NAME + " where " + QuestionAnswer.QuestionID + " = " + questionId;
        Cursor cursor = database.rawQuery(query, null);
        if (cursor == null || cursor.getCount() == 0)
            return questionModel;

        cursor.moveToFirst();
        do {
            questionModel.setQuestionId(cursor.getString(0));
            questionModel.setQuestion(cursor.getString(1));
            questionModel.setAnswerId(cursor.getString(2));

        } while (cursor.moveToNext());

        return questionModel;
    }*/

    public void deleteAllData() {
        try {
            database.delete(ContactDetailsTable.TABLE_NAME, null, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}