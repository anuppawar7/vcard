package com.etonius.vcard.helpers;

import android.content.Context;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import java.io.File;

/**
 * @author abhijeet.j
 */
public class FileOperationsHelper {

    public static final String IMAGES_FOLDER = Environment.getExternalStorageDirectory()
            + "/data/data/vcard/images/";

    public static void createFile(String filename) {
        try {
            File directory = new File(IMAGES_FOLDER);
            if (!directory.exists())
                directory.mkdirs();
            new File(directory, filename);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Delete images onCreate()
     */
    public static void deleteFolder() {
        try {
            File directory = new File(IMAGES_FOLDER);
            if (directory.isDirectory()) {
                String[] children = directory.list();
                for (int i = 0; i < children.length; i++) {
                    new File(directory, children[i]).delete();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String getQRCodeImage() {
        String QrcodeImage = null;
        try {
            File folder = new File("" + Environment.getExternalStorageDirectory() + "/VCard/QRCodes");
            if (folder.isDirectory()) {
                File[] files = folder.listFiles();
                if (files != null && files.length > 0) {
                    int numberOFFiles = files.length;
                    QrcodeImage = files[numberOFFiles - 1].getAbsolutePath();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return QrcodeImage;
    }
}
