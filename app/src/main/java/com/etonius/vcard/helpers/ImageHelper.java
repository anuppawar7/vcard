package com.etonius.vcard.helpers;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.*;
import android.media.ExifInterface;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.axelbuzz.malisamajmatrimony.utils.Utilities;
import com.etonius.vcard.R;
import com.etonius.vcard.models.MediaModel;
import com.etonius.vcard.utils.Constants;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageLoadingListener;
import com.squareup.picasso.Picasso;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author abhijeet.j
 * <p/>
 * Handles image intent call and reception
 */
public class ImageHelper {
    public static final String TAG = "Constraints";
    private Context context;

    private int imageHeight, imageWidth;
    private ImageLoader imageLoader;

    public void openCameraIntent() {
        Intent cameraIntent = new Intent(
                MediaStore.ACTION_IMAGE_CAPTURE);
        ((Activity) context).startActivityForResult(cameraIntent, Constants.Companion.getCAMERA_REQUEST());
    }

    /**
     * This method starts activity intent and this activity is returned in ImageUploadActivity
     */
    public void requestToGetImage(Context context) {

        // Determine Uri of camera image to save.
        final File root = new File(Environment.getExternalStorageDirectory() + File.separator + Constants.Companion.getIMAGES_FOLDER());
        root.mkdir();
        final String fname = "img_" + System.currentTimeMillis() + ".jpg";
        final File sdImageMainDirectory = new File(root, fname);
        Uri outputFileUri = Uri.fromFile(sdImageMainDirectory);
        final List<Intent> galleryIntents = new ArrayList<Intent>();
//
        final PackageManager packageManager = context.getPackageManager();
        Intent gallIntent = new Intent(Intent.ACTION_GET_CONTENT);
        gallIntent.setType("image/*");
        List<ResolveInfo> listGall = packageManager.queryIntentActivities(gallIntent, 0);
        for (ResolveInfo res : listGall) {
            final String packageName = res.activityInfo.packageName;
            final Intent intent = new Intent(gallIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(packageName);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            galleryIntents.add(intent);
        }

//        //FileSystem
        final Intent galleryIntent = new Intent();
        galleryIntent.setType("image/");
        galleryIntent.setAction(Intent.ACTION_GET_CONTENT);

        // Chooser of filesystem options.
        final Intent chooserIntent = Intent.createChooser(galleryIntent, "Select image");
        // Add the camera options.
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, galleryIntents.toArray(new Parcelable[]{}));
        ((Activity) context).startActivityForResult(chooserIntent, Constants.Companion.getPROFILE_REQUEST());
    }

    /**
     * This method starts activity intent and this activity is returned in ImageUploadActivity
     */
    public void requestToGetVideo(Context context) {
        Intent intent = new Intent();
        intent.setType("video/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        ((Activity) context).startActivityForResult(Intent.createChooser(intent, "Select Video"),
                Constants.Companion.getVIDEO_REQUEST());
    }

    public static Bitmap getBitmapFromFile(Context context, String filename, boolean isThumbnail) {
        Bitmap bitmap;
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        if (isThumbnail) {
            bitmap =
                    ThumbnailUtils.extractThumbnail(BitmapFactory.decodeFile(filename),
                            metrics.widthPixels,
                            (int) context.getResources().getDimension(R.dimen.gallery_image_height));
        } else {
            bitmap = BitmapFactory.decodeFile(filename);
        }
        preserveOrientation(filename, bitmap);

        return bitmap;
    }

    public static Bitmap getBitmapFromFile(String filename, Context context, boolean isThumbnail) {
        Bitmap bitmap;
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        if (isThumbnail) {
            bitmap =
                    ThumbnailUtils.extractThumbnail(BitmapFactory.decodeFile(filename),
                            metrics.widthPixels,
                            (int) context.getResources().getDimension(R.dimen.gallery_image_height));
        } else {
            bitmap = BitmapFactory.decodeFile(filename);
        }
        preserveOrientation(filename, bitmap);

        return bitmap;
    }

    public void getBitmapFromUrl(String path, final ImageView imageView, final ImageRenderListener imageRenderListener,
                                 final boolean isThumbnail) {

        imageLoader.loadImage(path, new ImageLoadingListener() {
            @Override
            public void onLoadingStarted(String imageUri, View view) {
                imageRenderListener.onStarted();
            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                if (imageRenderListener != null)
                    imageRenderListener.onImageRendered();
            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                if (isThumbnail)
                    imageView.setImageBitmap(ThumbnailUtils.extractThumbnail(loadedImage,
                            Utilities.Companion.getImageWidth(),
                            Utilities.Companion.getImageHeight()));
                else
                    imageView.setImageBitmap(loadedImage);


                if (imageRenderListener != null)
                    imageRenderListener.onImageRendered();
            }

            @Override
            public void onLoadingCancelled(String imageUri, View view) {
                imageRenderListener.onLoadingCancelled();
            }
        });
    }


  /*  public static void getBitmapFromUrl(final String imageUrl, final ImageBitmapListener imageBitmapListener) {
        Log.d(TAG, "getBitmapFromUrl() = " + imageUrl);
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    URL url = new URL(imageUrl);
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                    connection.setDoInput(true);
                    connection.connect();
                    InputStream input = connection.getInputStream();
                    Bitmap bitmap = BitmapFactory.decodeStream(input);
                    Log.d(TAG, "bitmap download complete");
                    imageBitmapListener.onImageDownloaded(bitmap);
                } catch (Exception e) {
                    Log.d(TAG, "getBitmapFromUrl exception: " + e.getLocalizedMessage());
                    e.printStackTrace();
                }
            }
        }).start();
    }*/

    public Bitmap getOriginalBitmapFromFile(String filename) {
        Bitmap bitmap = null;
        try {
//            File location = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/images");
//            File dest = new File(location, filename + ".JPG");
            File dest = new File(filename);
            FileInputStream fis;
            fis = new FileInputStream(dest);
            bitmap = BitmapFactory.decodeStream(fis);

//            Bitmap original = BitmapFactory.decodeFile(filename);
//
//            ByteArrayOutputStream out = new ByteArrayOutputStream();
//            original.compress(Bitmap.CompressFormat.JPEG, 60, out);
//            bitmap = BitmapFactory.decodeStream(new ByteArrayInputStream(out.toByteArray()));

            preserveOrientation(filename, bitmap);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return bitmap;
    }

    public interface ImageRenderListener {
        void onImageRendered();

        void onImageFailed();

        void onStarted();

        void onLoadingCancelled();
    }

    private static void preserveOrientation(String filename, Bitmap bitmap) {
        int rotate = 0;
        try {
            ExifInterface exif = new ExifInterface(filename);
            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_NORMAL);

            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotate = 270;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotate = 180;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotate = 90;
                    break;
            }

            System.gc();
            Matrix matrix = new Matrix();
            matrix.postRotate(rotate);
            bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(),
                    bitmap.getHeight(), matrix, true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public File getFileFromUri(Uri uri) {
        return new File(uri.getPath());
    }

   /* public String savePic(Bitmap bitmap, String filename) {
        FileOutputStream fos = null;
        String fileWrote = "";
        try {
            FileOperationsHelper.createFile(filename);
            fileWrote = Constants.IMAGES_FOLDER + filename;
            fos = new FileOutputStream(fileWrote);
            if (null != fos) {
                bitmap.compress(Bitmap.CompressFormat.JPEG, Constants.IMAGE_QUALITY, fos);
                fos.flush();
                fos.close();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return fileWrote;
    }*/

    public Bitmap compressImage(String imageUri) {

        String filePath = getRealPathFromURI(imageUri);
        Bitmap scaledBitmap = null;

        BitmapFactory.Options options = new BitmapFactory.Options();

//      by setting this field as true, the actual bitmap pixels are not loaded in the memory.
//      Just the bounds are loaded. If you try the use the bitmap here, you will get null.
        options.inJustDecodeBounds = true;
        Bitmap bmp = BitmapFactory.decodeFile(filePath, options);

        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;

//      max Height and width values of the compressed image is taken as 816x612

        float maxHeight = 816.0f;
        float maxWidth = 612.0f;
        float imgRatio = actualWidth / actualHeight;
        float maxRatio = maxWidth / maxHeight;

//      width and height values are set maintaining the aspect ratio of the image

        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                imgRatio = maxHeight / actualHeight;
                actualWidth = (int) (imgRatio * actualWidth);
                actualHeight = (int) maxHeight;
            } else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) maxWidth;
            } else {
                actualHeight = (int) maxHeight;
                actualWidth = (int) maxWidth;

            }
        }

//      setting inSampleSize value allows to load a scaled down version of the original image

        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);

//      inJustDecodeBounds set to false to load the actual bitmap
        options.inJustDecodeBounds = false;

//      this options allow android to claim the bitmap memory if it runs low on memory
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inTempStorage = new byte[16 * 1024];

        try {
//          load the bitmap from its path
            bmp = BitmapFactory.decodeFile(filePath, options);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();

        }
        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2,
                new Paint(Paint.FILTER_BITMAP_FLAG));

//      check the rotation of the image and display it properly
        ExifInterface exif;
        try {
            exif = new ExifInterface(filePath);

            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION, 0);
            Log.d("EXIF", "Exif: " + orientation);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 3) {
                matrix.postRotate(180);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 8) {
                matrix.postRotate(270);
                Log.d("EXIF", "Exif: " + orientation);
            }
            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0,
                    scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix,
                    true);
        } catch (IOException e) {
            e.printStackTrace();
        }

        FileOutputStream out = null;
        String filename = getFilename(filePath);
        try {
            out = new FileOutputStream(filename);

//          write the compressed bitmap at the destination specified by filename.
            scaledBitmap.compress(Bitmap.CompressFormat.JPEG, Constants.Companion.getIMAGE_QUALITY(), out);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return scaledBitmap;
    }

    public String getFilename(String filePath) {
        File file = new File(filePath);
        if (!file.exists()) {
            file.mkdirs();
        }
        String uriSting = (file.getAbsolutePath() + "/" + System.currentTimeMillis() + ".jpg");
        return uriSting;

    }

    private String getRealPathFromURI(String contentURI) {
        Uri contentUri = Uri.parse(contentURI);
        Cursor cursor = context.getContentResolver().query(contentUri, null, null, null, null);
        if (cursor == null) {
            return contentUri.getPath();
        } else {
            cursor.moveToFirst();
            int index = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(index);
        }
    }

    public int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        final float totalPixels = width * height;
        final float totalReqPixelsCap = reqWidth * reqHeight * 2;
        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++;
        }

        return inSampleSize;
    }

    public static Bitmap getResizedBitmap(Bitmap bm, int newWidth, int newHeight) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(scaleWidth, scaleHeight);

        // "RECREATE" THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(
                bm, 0, 0, width, height, matrix, false);
        bm.recycle();
        return resizedBitmap;
    }

    public interface ColorSelectionListener {
        void onColorSelected(int color);
    }

    public boolean isColorDark(int color) {
        double darkness = 1 - (0.299 * Color.red(color) + 0.587 * Color.green(color) + 0.114 * Color.blue(color)) / 255;
        if (darkness < 0.5) {
            return false; // It's a light color
        } else {
            return true; // It's a dark color
        }
    }


    public static void setMediaImage(ImageView img, Context context, int resourceId) {
        try {
            img.setImageResource(resourceId);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void setUserImage(ImageView img, Context context, List<MediaModel> mediaModels) {
        if (Utilities.Companion.isListEmpty(mediaModels)) {
            img.setImageResource(R.drawable.user);
        } else {
            try {
                String mediaUrl = mediaModels.get(0).getMediaUrl();
                if (Utilities.Companion.getString(mediaUrl).isEmpty())
                    img.setImageResource(R.drawable.user);
                else {
                    Picasso.get().load(mediaUrl).into(img);
//                    Glide.with(context).load(mediaUrl).into(img);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static void setMediaImage(ImageView imageView, Context context, MediaModel mediaModel, int defaultImage) {
        if (mediaModel == null) {
            imageView.setImageResource(defaultImage);
        } else {
            try {
                String mediaUrl = mediaModel.getMediaUrl();

                if (mediaUrl.indexOf("http://") != -1) {
                    Picasso.get().load(mediaUrl).into(imageView);
                } else {
                    imageView.setImageBitmap(getBitmapFromFile(context, mediaUrl, false));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static void setMediaImage(ImageView imageView, Context context, String mediaUrl, int defaultImage) {
        if (Utilities.Companion.isEmpty(mediaUrl)) {
            imageView.setImageResource(defaultImage);
        } else {
            try {
                if (mediaUrl.indexOf("http://") != -1) {
                    Picasso.get().load(mediaUrl).into(imageView);
                } else {
                    imageView.setImageBitmap(getBitmapFromFile(context, mediaUrl, false));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static void setMediaImage(ImageView img, Context context, List<MediaModel> mediaModels, int defaultImage) {
        if (Utilities.Companion.isListEmpty(mediaModels)) {
            img.setImageResource(defaultImage);
            return;
        }

        try {
            String mediaUrl = mediaModels.get(0).getMediaUrl();
            if (Utilities.Companion.getString(mediaUrl).isEmpty())
                img.setImageResource(R.drawable.ic_launcher_background);
            else {
                Picasso.get().load(mediaUrl).into(img);
//                    Glide.with(context).load(mediaUrl).into(img);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//
//    public void renderImage(Context context, ImageView imageView, String imagePath, ImageRenderListener imageRenderListener) {
//        this.context = context;
//        if (imagePath != null) {
//            if (imagePath.indexOf("http://") != -1) {
//                // Get image from url
//                Picasso.with(context).load(imagePath).into(imageView);
////                getBitmapFromUrl(imagePath, imageView, imageRenderListener, false);
//            } else {
//                // Get image locally
//                imageView.setImageBitmap(getBitmapFromFile(context, imagePath, false));
//                if (imageRenderListener != null)
//                    imageRenderListener.onImageRendered();
//            }
//        }
//    }
//
//    public void renderImage(Context context, ImageView imageView, MediaModel mediaModel, ImageRenderListener imageRenderListener) {
//        this.context = context;
//        String imagePath = mediaModel.getLocalImagePath();
//        if (Utilities.isEmpty(imagePath))
//            imagePath = mediaModel.getMediaUrl();
//
//        if (imagePath != null) {
//            if (imagePath.indexOf("http://") != -1) {
//                // Get image from url
//                getBitmapFromUrl(imagePath, imageView, imageRenderListener, false);
//            } else {
//                // Get image locally
//                imageView.setImageBitmap(getBitmapFromFile(context, imagePath, false));
//                if (imageRenderListener != null)
//                    imageRenderListener.onImageRendered();
//            }
//        }
//    }
//
//    public void renderImage(Context context, ImageView imageView, List<MediaModel> mediaModels, ImageRenderListener imageRenderListener) {
//        if (Utilities.isListEmpty(mediaModels)) {
//            imageView.setImageResource(R.drawable.app_icon);
//            return;
//        }
//
//        this.context = context;
//        MediaModel mediaModel = mediaModels.get(0);
//
//        String imagePath = mediaModel.getLocalImagePath();
//        if (Utilities.isEmpty(imagePath))
//            imagePath = mediaModel.getMediaUrl();
//
//        if (imagePath != null) {
//            if (imagePath.startsWith("http://") || imagePath.startsWith("https://")) {
//                // Get image from url
//                Picasso.with(context).load(imagePath).into(imageView);
//            } else {
//                // Get image locally
//                imageView.setImageBitmap(getBitmapFromFile(context, imagePath, false));
//                if (imageRenderListener != null)
//                    imageRenderListener.onImageRendered();
//            }
//        }
//    }
}
