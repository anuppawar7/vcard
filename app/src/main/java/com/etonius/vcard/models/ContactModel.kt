package com.etonius.vcard.models

import java.io.Serializable

class ContactModel : Serializable {
    var contactId = ""
    var contactName = ""
    var mobileNumber = ""
    var emailId = ""
    var photo = ""
    var address = ""
    var companyName = ""
    var companyLogo = ""
}
